<?php get_header(); ?>
<div class="container tour-taxo">
    <div id="content">
        <div class="inner-padding">
            <h1><?php $quried_obj = get_queried_object();   $head_title=ucfirst($quried_obj->name); echo $head_title;?></h1>
            <?php
                $i=1;
                global $wp_query;
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                if (have_posts()):                     
                    while (have_posts()): the_post();  
                        $found_post = $wp_query->found_posts;
                        $tour_title=get_the_title();
                        $tour_per=get_permalink();
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full');
                        $newimg = aq_resize($image[0], 767, 575, true, true, true);
                        $metas = get_fields();
                        if($i==1){echo '<div class="row">';}
            ?>	
                        <div class="col-xs-12 col-sm-6 col-md-6"> 
                            <div class="taxo_content">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="tour-img">
                                            <a href="<?php echo $tour_per?>" title="<?php echo $tour_title;?>"><img class="img-responsive" src="<?php  echo $newimg; ?>" alt="<?php echo $tour_title;?>"/></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="tour-details">
                                            <h3><a href="<?php echo $tour_per;?>" title="<?php echo $tour_title;?>"><?php echo $tour_title;?></a></h3>
                                            <?php if($metas['tour_code']):?>
                                                <div>- Tour Code - <?php echo $metas['tour_code'];?></div>
                                            <?php endif?>

                                            <?php if($metas['duration']):?>
                                                <div class="btn btn-grey"><i class="fa fa-clock-o"></i> <?php echo $metas['duration'];?></div>
                                            <?php endif?>

                                            <?php if($metas['price']):?>
                                                <div class="price btn btn-grey">
                                                    <?php
                                                        $user = wp_get_current_user();
                                                        // var_dump($user->roles[0]);var_dump($user->data->user_login);                                                
                                                        if ( $user->roles[0] == 'subscriber') {
                                                            $price=$metas['agent_price'];
                                                        }
                                                        else {                                                                
                                                            $price=$metas['price'];
                                                        }
                                                    ?>
                                                    $<?php echo $price;?>	                                    
                                                </div>
                                            <?php endif?>  
                                        </div>
                                    </div>
                                    <div class="view_tour">
                                        <a class="btn btn-blue" href="<?php echo $tour_per?>" title="View <?php echo $tour_title;?>">View Tour</a>
                                    </div>
                                </div>
                            </div>
                        </div>															
            <?php
                    if ($i == 2 || $found_post == 1) { echo '</div> <!-- aa-->';$i=0; }
                    $i++;
                endwhile;
            ?>
            <?php
                endif;
                wp_reset_query();
            ?>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <?php dd_pagination(); ?>
                        </ul>
                    </nav>
                </div>
            </div>
            
        </div>
    </div>
</div><!-- /container -->
<?php get_footer();
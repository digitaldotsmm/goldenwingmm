<?php get_header(); ?>
<div class="container">
    <div id="content">
        <div class="inner-padding all_destinations">
            <div class="row">
                <div class="col-md-12">
                    <h1>All destinations</h1>
                    <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        $destinations = query_posts(array(
                                'orderby'          => 'post_date',
                                'order'            => 'DESC',
                                'post_status'      => 'publish',
                                'post_type'        => GW_DESTINATION,
                                'paged'            => $paged,
                                'posts_per_page'   => 12
                        ));
                        if($destinations):
                        foreach($destinations as $destination):
                        $des_title=  $destination->post_title;
                        $des_per=  get_the_permalink($destination->ID);
                        $img= wp_get_attachment_image_src(get_post_thumbnail_id($destination->ID), 'full');
                        $img_url= aq_resize($img[0],767,575,true,true,true);
                        if($img_url){
                            $img_url=$img_url;
                        }else{
                            $img_url= ASSET_URL.'images/logo-2.jpg';
                        }                         
                    ?> 
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 mb30"> 
                            <div class="des-content"> 
                                <div class="des-img"> 
                                    <a href="<?php echo $des_per;?>" title="<?php echo $des_title;?>"><img class="img-responsive" src="<?php echo $img_url;?>" alt="<?php echo $des_title;?>"></a> 
                                </div> 
                                <div class="des-details"> 
                                    <h3><a href="<?php echo $des_per;?>" title="<?php echo $des_title;?>"><?php echo $des_title;?></a></h3>
                                </div>
                            </div> 									
                            <p><?php echo substr($destination->post_content,0,120);?>...</p>
                            <a class="know-more" href="<?php echo $des_per;?>"><strong>View Details</strong></a>
                        </div>
                    <?php endforeach;?>
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <?php dd_pagination(); ?>
                            </ul>
                        </nav>
                    </div>	
                     <?php endif; ?>
                <div class="clear"></div>
                </div><!-- /col - 12 end -->
            </div> <!-- Row End -->
        </div>			
    </div>
</div>
<?php get_footer(); ?>
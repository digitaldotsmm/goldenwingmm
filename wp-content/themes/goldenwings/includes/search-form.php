<form action="<?php echo get_permalink(GW_SEARCH_TOUR); ?>" method="POST">
    <div class="radio">
        <h3>WHERE TO GO:</h3>
        <?php 
            $hot_term_id = pll_get_term(GW_HOT_DEAL_TOUR_TERM_ID);
            $tour_types = get_terms( array('taxonomy' => GW_TOUR_TYPE_TAXO,'parent'   => 0,'exclude'    => $hot_term_id ) );
            foreach ($tour_types as $ttkey=>$tour_type) {
//                var_dump($tour_type);die;
        ?>
                <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios<?php echo $ttkey+1;?>" value="<?php echo $tour_type->term_id;?>" <?php echo ( $ttkey==0)?'checked':'';?> >                   
                    <?php echo $tour_type->name;?>    
                </label>
        <?php        
            }
        ?>                                                                                     
    </div>
    <div class="search-select">
        <div class="inbound_child">
            <h3>TOUR TYPE <?php do_action ( 'change_terms' );?></h3>
            <select name="inbound_type" class="form-control">
                <option value="none">Select Tour Type</option>
                <?php 
                    $inbound_term_id = pll_get_term(GW_INBOUND_TOUR_TERM_ID);
                    $inboundchildren = get_term_children( $inbound_term_id, GW_TOUR_TYPE_TAXO );              
                    foreach ( $inboundchildren as $ibkey=>$inboundchilds ) : 
                        $inboundchild = get_term_by( 'id', $inboundchilds, GW_TOUR_TYPE_TAXO ); 
                ?>
                        <option value="<?php echo $inboundchild->term_id?>"><?php echo $inboundchild->name?></option>
                <?php endforeach;?>
            </select>
        </div>

        <div class="outbound_child">
            <h3>LOCATION</h3>
            <select name="outbound_type" class="form-control">
                <option value="none">Select Location</option>
                <?php 
                    $outbound_term_id = pll_get_term(GW_OUTBOUND_TOUR_TERM_ID);
                    $outboundchildren = get_term_children( $outbound_term_id, GW_TOUR_TYPE_TAXO );              
                    foreach ( $outboundchildren as $ibkey=>$outboundchilds ) : 
                        $outboundchild = get_term_by( 'id', $outboundchilds, GW_TOUR_TYPE_TAXO ); 
                ?>
                        <option value="<?php echo $outboundchild->term_id?>"><?php echo $outboundchild->name?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-default">SEARCH</button>
</form>
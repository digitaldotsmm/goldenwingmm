<?php /* Template Name: Search Tours */ ?>
<?php get_header(); ?>
<div class="container">
    <div id="content" class="search_tour">
        <div class="inner-padding">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        $tour_type=$_POST['optionsRadios'];
//                        $tour_type=pll_get_term($tour_type);
                        $inbound_type=$_POST['inbound_type'];                        
                        $outbound_type=$_POST['outbound_type'];                        
                        if($inbound_type == 'none') {
                                $tour_term= $outbound_type;
//                                $tour_term = pll_get_term($tour_term);
                        }else {
                                $tour_term= $inbound_type;
//                                $tour_term = pll_get_term($tour_term);
                        }
                        if($tour_term==GW_TREKKING_TOUR_TERM_ID || $tour_term==GW_TREKKING_TOUR_TERM_MM_ID) {
                            $search_tours = get_posts(
                                array(
                                    'post_type' => GW_TOUR_PROGRAM,
                                    'posts_per_page' => -1,
                                    'paged'            => $paged,
                                    'tax_query' => array(
                                                    'relation' => 'AND',
                                                    array(
                                                            'taxonomy' => GW_TOUR_TYPE_TAXO,
                                                            'field'    => 'term_id',
                                                            'terms'    => $tour_type,
                                                    ),
                                                    array(
                                                            'taxonomy' => GW_TOUR_TYPE_TAXO,
                                                            'field'    => 'term_id',
                                                            'terms'    => $tour_term
                                                    ),
                                            ),
                                    )
                            );
                            $location_taxos = get_terms(GW_LOCATION_TAXO);
                        ?>
                            <div id="tourtab" class="vertical_tabs treeking_tours" data-type="default">
                                <ul class="resp-tabs-list">  
                                <?php
                                        foreach ($location_taxos as $key => $location_taxo) {
                                            echo '<li>'.$location_taxo->name.'</li>';
                                        }
                                ?>
                                </ul>
                                <div class="resp-tabs-container"> 
                                    <?php 
                                        foreach ($location_taxos as $key => $location_taxo) {
                                            echo '<div><div class="row">';
                                                $trek_tours = get_posts(
                                                                array(
                                                                    'post_type' => GW_TOUR_PROGRAM,
                                                                    'posts_per_page' => -1,
                                                                    'paged'            => $paged,
                                                                    'tax_query' => array(
                                                                        'relation' => 'AND',
                                                                            array(
                                                                                    'taxonomy' => GW_TOUR_TYPE_TAXO,
                                                                                    'field'    => 'term_id',
                                                                                    'terms'    => $tour_type,
                                                                            ),
                                                                            array(
                                                                            'relation' => 'AND',
                                                                                array(
                                                                                    'taxonomy' => GW_TOUR_TYPE_TAXO,
                                                                                    'field'    => 'term_id',
                                                                                    'terms'    => $tour_term,
                                                                                ),
                                                                                array(
                                                                                    'taxonomy' => GW_LOCATION_TAXO,
                                                                                    'field'    => 'slug',
                                                                                    'terms'    => $location_taxo->slug,
                                                                                ),
                                                                            ),
                                                                        ),
                                                                    )
                                                                );
                                                foreach ($trek_tours as  $trek_tour) {                                   
                                                    $tour_title=$trek_tour->post_title;
                                                    $tour_per=get_permalink($trek_tour->ID);
                                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id($trek_tour->ID), 'full');
                                                    $newimg = aq_resize($image[0], 600, 400, true, true, true);
                                    ?>
                                                        <div class="col-xs-12 col-sm-6"> 
                                                            <div class="taxo_content">
                                                                <div class="tour-img">
                                                                    <a href="<?php echo $tour_per?>" title="<?php echo $tour_title;?>"><img class="img-responsive" src="<?php  echo $newimg; ?>" alt="<?php echo $tour_title;?>"/></a>
                                                                </div>

                                                                <div class="tour-details">
                                                                    <h3><a href="<?php echo $tour_per;?>" title="<?php echo $tour_title;?>"><?php echo $tour_title;?></a></h3>
                                                                    <?php if(get_field('duration',$trek_tour->ID,true)):?>
                                                                            <div class="btn btn-grey"><i class="fa fa-clock-o"></i> <?php echo get_field('duration',$trek_tour->ID,true);?></div>
                                                                    <?php endif?>

                                                                    <?php if(get_field('price',$trek_tour->ID,true)):?>
                                                                        <div class="price btn btn-grey">
                                                                            <?php
                                                                                $user = wp_get_current_user();
                                                                                // var_dump($user->roles[0]);var_dump($user->data->user_login);
                                                                                if ( $user->roles[0] == 'administrator' && $user->data->user_login == 'zeus') {
                                                                                    $price=get_field('price',$trek_tour->ID,true);
                                                                                }
                                                                                else {
                                                                                    $price=get_field('agent_price',$trek_tour->ID,true);
                                                                                }
                                                                            ?>
                                                                            $<?php echo $price;?>	                                    
                                                                        </div>
                                                                    <?php endif?>
                                                                    <div class="view_tour">
                                                                        <a class="btn btn-blue" href="<?php echo $tour_per?>">View Tour</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>	
                                    <?php		
                                                }
                                        echo '</div></div>';
                                        } // location taxo endforeach
                                    ?>
                            </div>
                        </div>
                    <?php
                        }// end of if trekkng tour
                        else {
//                            var_dump($tour_type);
//                                                    var_dump($tour_term);die;
                            $search_term = get_term_by('id', $tour_term, GW_TOUR_TYPE_TAXO);
//                                                    var_dump($search_term);
                    ?>
                            <h1>Search - <?php echo ucwords($search_term->name);?></h1>					
                            <?php 												
                                $search_tours = null;
                                $search_tours = get_posts(
                                    array(
                                        'post_type' => GW_TOUR_PROGRAM,
                                        'posts_per_page' => -1,
                                        'paged'            => $paged,
                                        'tax_query' => array(
                                            'relation' => 'AND',
                                                array(
                                                        'taxonomy' => GW_TOUR_TYPE_TAXO,
                                                        'field'    => 'term_id',
                                                        'terms'    => $tour_type,
                                                ),
                                                array(
                                                        'taxonomy' => GW_TOUR_TYPE_TAXO,
                                                        'field'    => 'term_id',
                                                        'terms'    => $tour_term
                                                ),
                                        ),
                                    )
                                );        
//                                var_dump($tour_type);
//                                var_dump($tour_term);
//                                var_dump($search_tours);die;
                            $h=1;
                            $hwrap_div='<div class="row">';
                            if($search_tours) :
                                echo '<div class="row">';
                                foreach ($search_tours as $search_tour) :
                                    $tour_per=get_permalink($search_tour->ID);
                                    $himage = wp_get_attachment_image_src( get_post_thumbnail_id( $search_tour->ID ), 'full');
                                    $newimg = aq_resize($himage[0], 600, 400, true, true, true);
                                    $tour_title = $search_tour->post_title;
                        ?>
                        			<div class="col-xs-12 col-sm-6 col-md-6"> 
                                                    <div class="taxo_content">
                                                        <div class="tour-img">
                                                            <a href="<?php echo $tour_per?>" title="<?php echo $tour_title;?>"><img class="img-responsive" src="<?php  echo $newimg; ?>" alt="<?php echo $tour_title;?>"/></a>
                                                        </div>
			                            
                                                        <div class="tour-details">
                                                            <h3><a href="<?php echo $tour_per;?>" title="<?php echo $tour_title;?>"><?php echo $tour_title;?></a></h3>			                                
                                                            <?php if(get_field('duration',$search_tour->ID,true)):?>
                                                                <div class="btn btn-grey"><i class="fa fa-clock-o"></i> <?php echo get_field('duration',$search_tour->ID,true);?></div>
                                                            <?php endif?>

                                                                <?php if(get_field('price')):?>
                                                                    <div class="price btn btn-grey">
                                                                    <?php
                                                                        $user = wp_get_current_user();
                                                                        // var_dump($user->roles[0]);var_dump($user->data->user_login);
                                                                        if ( $user->roles[0] == 'administrator' && $user->data->user_login == 'zeus') {
                                                                            $price=get_field('price',$search_tour->ID,true);
                                                                        }
                                                                        else {
                                                                            $price=get_field('agent_price',$search_tour->ID,true);
                                                                        }
                                                                    ?>
                                                                    $<?php echo $price;?>	                                    
                                                                    </div>
                                                            <?php endif?>
                                                            <div class="view_tour">
                                                                    <a class="btn btn-blue" href="<?php echo $tour_per?>">View Tour</a>
                                                            </div>
                                                        </div>
			                            </div>
                                                </div>	
                    <?php 
                            //                                  Is this a fourth post? If so, make sure it is not the last post?
                            if ($h % 2 === 0 ) { echo '</div>' . $hwrap_div; }
                            $h++;
                            endforeach;
                            echo '</div>';
                        endif;
                    ?> 
                    <?php } ?> <!-- not treeking -->       				
					<div class="clear"></div>
				</div><!-- /col - 8 end -->
			</div> <!-- Row End -->
		</div>
	</div>
</div>
	<!-- /container -->
<?php get_footer(); ?>
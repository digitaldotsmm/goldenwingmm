<?php get_header(); ?>
        <div class="container">
            <div id="content">
                <div class="row home-slider">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">                     
                          <!-- Wrapper for slides -->
                          <div class="carousel-inner" role="listbox">
                            <?php $slider = get_field('slide'); 
//                            var_dump($slider);die;
                            if($slider): 
                                foreach ($slider as $key => $slide) {
                                
                                        $img = aq_resize($slide['image'],861,422,true,true,true);
                                        $slider_title=$slide['slider_title'];
                             ?>
                            <div class="item<?php echo($key == null)?' active':''; ?>">
                              <img  src="<?php  echo $img ; ?>" alt="Golden Wing Myanmar - <?php echo $slider_title; ?>">
                              <div class="carousel-caption">
                                <h3><?php echo $slider_title; ?></h3>
                              </div>
                            </div>
                            <?php } endif; ?>
                           </div>
                        
                          <!-- la -->
                          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 mobile_mb30">
                        <div class="h-tab">
                            <div id="left-slider">
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">YOUR TRAVEL CHOICE</a></li>
                                <!--<li role="presentation" class="last"><a href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">AGENT LOG IN</a></li>-->
                              </ul>
                            
                              <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="tab-1">
                                        <?php include_once 'includes/search-form.php' ?>
                                    </div>
<!--                                    <div role="tabpanel" class="tab-pane" id="tab-2">
                                        <?php //echo do_shortcode('[theme-my-login]'); ?>
                                        <a href="<?php //echo get_permalink(GW_REGISTER);?>" class="user_register" title="Not registered?">Register</a>   
                                    </div>                               -->
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- end Slider -->
                
                <div class="all-tours">
                    <div class="hot-tours home-tours">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="title-tours">
                                    <img class="img-responsive" src="<?php  echo ASSET_URL ?>images/icon-title-1.jpg" alt="" />
                                    <h2>HOT DEAL TOURS</h2>                                    
                                </div>
                            </div>
                        </div>                      
                        <?php 
                            $hot_term_id = pll_get_term(GW_HOT_DEAL_TOUR_TERM_ID);
                            
                            $hot_deal_tours = null;
                            $hot_deal_tours = get_posts(
                                array(
                                    'post_type' => GW_TOUR_PROGRAM,
                                    'posts_per_page' => 6,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => GW_TOUR_TYPE_TAXO,
                                            'field' => 'term_id',
                                            'terms' => $hot_term_id,
                                        )
                                    )
                                )
                            );

                            $h=1;
                            $hwrap_div='<div class="row">';
                            if($hot_deal_tours) :
                                echo '<div class="row">';
                                foreach ($hot_deal_tours as $hot_deal_tour) :
                                    $hot_per=get_permalink($hot_deal_tour->ID);
                                    $himage = wp_get_attachment_image_src( get_post_thumbnail_id( $hot_deal_tour->ID ), 'full');
                                    $hnewimg = aq_resize($himage[0], 333, 222, true, true, true);
                        ?>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="tour-content">
                                            <div class="tour-img">
                                                <a href="<?php echo $hot_per?>" title="<?php echo $hot_deal_tour->post_title;?>"><img class="img-responsive" src="<?php echo $hnewimg;?>" alt="<?php echo $hot_deal_tour->post_title;?>"/></a>
                                            </div>
                                            <div class="tour-details">
                                                <h3><a href="<?php echo $hot_per;?>" title="<?php echo $hot_deal_tour->post_title;?>"><?php echo $hot_deal_tour->post_title;//echo substr($hot_deal_tour->post_title,0,23);?></a></h3>                                                
                                                <?php
                                                    $user = wp_get_current_user();
                                                    // var_dump($user->roles[0]);var_dump($user->data->user_login);
                                                    // if ( $user->roles[0] == 'administrator' && $user->data->user_login == 'zeus') {
                                                    //     $price=get_field('price',$hot_deal_tour->ID,true);
                                                    // }
                                                    // else {
                                                    //     $price=get_field('agent_price',$hot_deal_tour->ID,true);
                                                    // }
                                                    if ( $user->roles[0] == 'subscriber') {
                                                        $price=get_field('agent_price',$hot_deal_tour->ID,true);
                                                        if($price) {
                                                            $price= '<span class="price">$'.$price.'</span>';
                                                        }else {
                                                            $price= '';
                                                        }                                                      
                                                    }
                                                    else {                                                                
                                                        $price=get_field('price',$hot_deal_tour->ID,true);
                                                        if($price) {
                                                            $price= '<span class="price">$'.$price.'</span>';
                                                        }else {
                                                            $price= '';
                                                        }
                                                    }

                                                ?>
                                                <?php echo $price;?>                                                   
                                            </div>
                                       </div>
                                    </div>
                        <?php 
                                //                                  Is this a fourth post? If so, make sure it is not the last post?
                                if ($h % 3 === 0 ) { echo '</div>' . $hwrap_div; }
                                $h++;
                                endforeach;
                                echo '</div>';
                            endif;
                        ?>                              
                        <div class="row"><div class="col-xs-12 view_all"><a href="/<?php echo GW_TOUR_TYPE_TAXO;?>/<?php echo GW_HOT_DEAL_TOUR_TERM;?>/" class="btn btn-darkyellow">View All</a></div></div>
                        <div class="double-solid"></div>
                    </div>
                    <!-- end hot tours -->
                    <?php
                        $trekking_term_id = pll_get_term(GW_TREKKING_TOUR_TERM_ID);
                    	$trekking_tours = null;
                        $trekking_tours = get_posts(
                            array(
                                'post_type' => GW_TOUR_PROGRAM,
                                'posts_per_page' => 2,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => GW_TOUR_TYPE_TAXO,
                                        'field' => 'term_id',
                                        'terms' => $trekking_term_id,
                                    )
                                )
                            )
                        );
                        $bike_term_id = pll_get_term(GW_BIKE_TOUR_TERM_ID);
                        $bike_tours = null;
                                $bike_tours = get_posts(
                                    array(
                                        'post_type' => GW_TOUR_PROGRAM,
                                        'posts_per_page' => 2,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => GW_TOUR_TYPE_TAXO,
                                                'field' => 'term_id',
                                                'terms' => $bike_term_id,
                                            )
                                        )
                                    )
                                );

                        if($trekking_tours || $bike_tours)  {
                    ?>
                    <div class="trekking-bike">
                        <div class="row">
                        	<?php
		                        if($trekking_tours) :
		                    ?>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="title-tours">
                                            <img class="img-responsive" src="<?php  echo ASSET_URL ?>images/icon-title-2.jpg" alt="" />
                                            <h2>TREKKING TOURS</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php                                         
                                            foreach ($trekking_tours as $trekking_tour) :
                                                $trekk_per=get_permalink($trekking_tour->ID);
                                                $timage = wp_get_attachment_image_src( get_post_thumbnail_id( $trekking_tour->ID ), 'full');
                                                $tnewimg = aq_resize($timage[0], 243, 162, true, true, true);
                                    ?>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="tour-content">
                                                        <div class="tour-img">
                                                            <?php 
                                                                // vars 
                                                                $new_tour = get_field('new_tour_check',$trekking_tour->ID,true);
                                                                // check
                                                                if( $new_tour && in_array('Yes', $new_tour) ):                                                                                                         
                                                            ?>
                                                                <div class="tour-new"><img class="img-responsive" src="<?php echo ASSET_URL; ?>images/tour-new.png" alt="<?php echo $trekking_tour->post_title;?>" /></div>
                                                            <?php endif;?>
                                                            <a href="<?php echo $trekk_per;?>" title="<?php echo $trekking_tour->post_title;?>"><img class="img-responsive" src="<?php  echo $tnewimg ?>" alt=""/></a>
                                                            <h3><a href="<?php echo $trekk_per;?>" title="<?php echo $trekking_tour->post_title;?>"><?php echo $trekking_tour->post_title; ?></a></h3>
                                                        </div>
                                                        <div class="tour-details"> 
                                                            <?php
                                                                $user = wp_get_current_user();
                                                                // var_dump($user->roles[0]);var_dump($user->data->user_login);                                                                   
                                                                if ( $user->roles[0] == 'subscriber') {
                                                                    $price=get_field('agent_price',$trekking_tour->ID,true);
                                                                    if($price) {
                                                                        $price= '<span class="price">$'.$price.'</span>';
                                                                    }else {
                                                                        $price= '';
                                                                    }    
                                                                }
                                                                else {                                                                
                                                                    $price=get_field('price',$trekking_tour->ID,true);
                                                                    if($price) {
                                                                        $price= '<span class="price">$'.$price.'</span>';
                                                                    }else {
                                                                        $price= '';
                                                                    }    
                                                                }
                                                            ?>
                                                            <?php echo $price;?> 
                                                            <a href="<?php echo $trekk_per;?>">Details</a>
                                                        </div>
                                                    </div>
                                                </div>
                                    <?php 
                                            endforeach;                                        
                                    ?>   
                                </div>
                            </div><!-- end -->
                            <?php endif;?>
                            <?php 
                                if($bike_tours) :
                            ?>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="title-tours">
                                            <img class="img-responsive" src="<?php  echo ASSET_URL ?>images/icon-title-3.jpg" alt="" />
                                            <h2>BIKE TUORS</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php                                        
                                            foreach ($bike_tours as $bike_tour) :
                                                $bike_per=get_permalink($bike_tour->ID);
                                                $bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $bike_tour->ID ), 'full');
                                                $bnewimg = aq_resize($bimage[0], 243, 162, true, true, true);
                                    ?>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="tour-content">
                                                        <div class="tour-img">
                                                            <?php 
                                                                // vars 
                                                                $new_tour = get_field('new_tour_check',$bike_tour->ID,true);
                                                                // check
                                                                if( $new_tour && in_array('Yes', $new_tour) ):                                                                                                         
                                                            ?>
                                                                <div class="tour-new"><img class="img-responsive" src="<?php echo ASSET_URL; ?>images/tour-new.png" alt="<?php echo $bike_tour->post_title;?>" /></div>
                                                            <?php endif;?>
                                                            <a href="<?php echo $trekk_per;?>" title="<?php echo $bike_tour->post_title;?>"><img class="img-responsive" src="<?php echo $bnewimg; ?>" alt="<?php echo $bike_tour->post_title;?>"/></a>
                                                            <h3><a href="<?php echo $trekk_per;?>" title="<?php echo $bike_tour->post_title;?>"><?php echo $bike_tour->post_title; ?></a></h3>
                                                        </div>
                                                        <div class="tour-details"> 
                                                            <?php if(get_field('price',$bike_tour->ID,true)):?>
                                                                <span class="price">
                                                                <?php
                                                                    $user = wp_get_current_user();
                                                                    // var_dump($user->roles[0]);var_dump($user->data->user_login);                                                                    
                                                                    if ( $user->roles[0] == 'subscriber') {
                                                                        $price=get_field('agent_price',$bike_tour->ID,true);
                                                                    }
                                                                    else {                                                                
                                                                        $price=get_field('price',$bike_tour->ID,true);
                                                                    }
                                                                ?>
                                                                $<?php echo $price;?>
                                                                </span>
                                                            <?php endif;?>
                                                            <a href="<?php echo $bike_per;?>" title="<?php echo $bike_tour->post_title;?>">Details</a>
                                                        </div>
                                                    </div>
                                                </div>
                                    <?php 
                                            endforeach;                                       
                                    ?>  
                                </div>
                            </div><!-- end -->
                            <?php  endif;?>
                        </div>
                        <div class="double-solid"></div>
                    </div>
                    <?php } ?>
                    <!-- end trekking & bike -->
                    <div class="trekking-bike">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="title-tours">
                                    <img class="img-responsive" src="<?php  echo ASSET_URL ?>images/icon-title-4.jpg" alt="" />
                                    <h2>LATEST TOURS</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php 
                                $latest_tours = null;
                                $latest_tours = get_posts(
                                    array(
                                        'post_type' => GW_TOUR_PROGRAM,
                                        'posts_per_page' => 4,
                                    )
                                );
                                // var_dump($latest_tours);die;
                                if($latest_tours) :
                                    foreach ($latest_tours as $latest_tour) :
                                        $latest_per=get_permalink($latest_tour->ID);
                                        $limage = wp_get_attachment_image_src( get_post_thumbnail_id( $latest_tour->ID ), 'full');
                                        $lnewimg = aq_resize($limage[0], 243, 162, true, true, true);
                            ?>
                                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                            <div class="tour-content">
                                                <div class="tour-img">
                                                    <?php 
                                                        // vars 
                                                        $new_tour = get_field('new_tour_check',$latest_tour->ID,true);
                                                        // check
                                                        if( $new_tour && in_array('Yes', $new_tour) ):                                                                                                         
                                                    ?>
                                                        <div class="tour-new"><img class="img-responsive" src="<?php echo ASSET_URL; ?>images/tour-new.png" alt="<?php echo $latest_tour->post_title;?>" /></div>
                                                    <?php endif; ?>
                                                    <a href="<?php echo $latest_per; ?>" title="<?php echo $latest_tour->post_title;?>"><img class="img-responsive" src="<?php echo $lnewimg; ?>" alt="<?php echo $latest_tour->post_title;?>"/></a>
                                                    <h3><a href="<?php echo $latest_per; ?>" title="<?php echo $latest_tour->post_title;?>"><?php echo $latest_tour->post_title; ?></a></h3>
                                                </div>
                                                <div class="tour-details"> 
                                                    <?php if(get_field('price',$latest_tour->ID,true)):?>
                                                        <span class="price">
                                                        <?php
                                                            $user = wp_get_current_user();
                                                            // var_dump($user->roles[0]);var_dump($user->data->user_login);
                                                            if ( $user->roles[0] == 'administrator' && $user->data->user_login == 'zeus') {
                                                                $price=get_field('price',$latest_tour->ID,true);
                                                            }
                                                            else {
                                                                $price=get_field('agent_price',$latest_tour->ID,true);
                                                            }
                                                        ?>
                                                        $<?php echo $price;?>
                                                        </span>
                                                    <?php endif;?>
                                                    <a href="<?php echo $latest_per; ?>" title="<?php echo $latest_tour->post_title;?>">Details</a>
                                                </div>
                                            </div>
                                        </div>
                            <?php 
                                    endforeach;
                                endif;
                            ?> 
                            
                        </div>
                    </div>
                </div>
                <!-- end Tours -->
            </div>
        </div>
        <!-- end Content -->
<?php get_footer(); 
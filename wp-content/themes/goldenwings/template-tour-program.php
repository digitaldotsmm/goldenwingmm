<?php /* Template Name: Tour Program */ ?>
<?php get_header(); ?>
<div class="container tour-taxo">
	<div id="content">
		<div class="inner-padding">
		<h1><?php $quried_obj = get_queried_object();   $head_title=ucfirst($quried_obj->name); echo $head_title;?></h1>
			<?php
				$i=1;
                $wrap_div='<div class="row">';
                if (have_posts()): 
                     
                    echo '<div class="row">';
                 
                    while (have_posts()): the_post();  
                    	$tour_title=get_the_title();
                    	$tour_per=get_permalink();
                    	$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full');
                        $newimg = aq_resize($image[0], 350, 233, true, true, true);
                        // var_dump($newimg);
        	?>	
    					<div class="col-xs-12 col-sm-6 col-md-6"> 
                            <div class="taxo_content">
    							<div class="col-md-6 col-sm-12 col-xs-12 tour-img">
                                	<a href="<?php echo $tour_per?>" title="<?php echo $tour_title;?>"><img class="img-responsive" src="<?php  echo $newimg; ?>" alt="<?php echo $tour_title;?>"/></a>
                                </div>
                            
                                <div class="col-md-6 col-sm-12 col-xs-12 tour-details">
                                    <h3><a href="<?php echo $tour_per;?>" title="<?php echo $tour_title;?>"><?php echo $tour_title;?></a></h3>

                                    <?php if(get_field('tour_code')):?>
    	                                <div>
    		                            	- Tour Code - <?php echo get_field('tour_code');?>
    		                            </div>
    	                        	<?php endif?>

    	                        	<?php if(get_field('duration')):?>
    	                            	<div class="btn btn-grey"><i class="fa fa-clock-o"></i> <?php echo get_field('duration');?></div>
                                	<?php endif?>

    	                            <?php if(get_field('price')):?>
    	                                <div class="price btn btn-grey">
    	                                    <?php
                                                $user = wp_get_current_user();
                                                // var_dump($user->roles[0]);var_dump($user->data->user_login);
                                                if ( $user->roles[0] == 'subscriber') {
                                                    $price=get_field('agent_price',$post->ID,true);
                                                }
                                                else {                                                                
                                                    $price=get_field('price',$post->ID,true);
                                                }
                                            ?>
                                            $<?php echo $price;?>
    	                                </div>
                                    <?php endif?>
                                    <div class="view_tour">
                                    	<a class="btn btn-blue" href="<?php echo $tour_per?>">View Tour</a>
                                    </div>
                                </div>
                            </div>
        				</div>															
			<?php
//                                  Is this a fourth post? If so, make sure it is not the last post?
                        if ($i % 2 === 0 ) { echo '</div>' . $wrap_div; }
                        $i++;

                     endwhile;
                   
                    echo '</div>';
                   
                endif;
                wp_reset_query();
            ?>
			
		</div>
	</div>
</div>
	<!-- /container -->
<?php get_footer(); ?>
<?php
define('TEMPLATE_URL', get_stylesheet_directory_uri());
define('ASSET_URL', TEMPLATE_URL . '/assets/');
require_once('includes/aq_resizer.php');
// Register custom navigation walker
//require_once('includes/wp_bootstrap_navwalker.php');

/* Custom post and page */
define('GW_HOME', 2);
define('GW_ABOUT_MYANMAR', 521);
define('GW_FLIGHT_INFORMATIONS', 40);
define('GW_TRAVEL_INFORMATIONS', 469);
define('GW_TRAVLE_TIPS', 935);
define('GW_CONTACT_US', 934);
define('GW_DO_DONT', 5399);
define('GW_HOTELS_IN_MYANMAR', 936);
define('GW_RESERVATION', 581);
define('GW_SEARCH_TOUR', 5593);
define('GW_REGISTER', 5437);

define('GW_DESTINATION', 'destination');
define('GW_TOUR_PROGRAM', 'tour-program');
define('GW_HOTEL', 'hotel');
// taxo and terms
define('GW_TOUR_TYPE_TAXO', 'tour-type');
define('GW_HOTEL_LOCATION_TAXO', 'hotel-location');
define('GW_LOCATION_TAXO', 'location');

define('GW_INBOUND_TOUR_TERM', 'inbound-tours');
define('GW_INBOUND_TOUR_TERM_ID', 27);
define('GW_OUTBOUND_TOUR_TERM', 'outbound-tours');
define('GW_OUTBOUND_TOUR_TERM_ID', 28);
define('GW_DOMESTIC_TOUR_TERM', 'domestic-package');
define('GW_HOT_DEAL_TOUR_TERM', 'hot-deal-tours');
define('GW_HOT_DEAL_TOUR_TERM_ID', 31);
define('GW_BIKE_TOUR_TERM', 'bike-tours');
define('GW_BIKE_TOUR_TERM_ID', 30);
define('GW_TREKKING_TOUR_TERM', 'trekking-tours');
define('GW_TREKKING_TOUR_TERM_ID', 29);
define('GW_TREKKING_TOUR_TERM_MM_ID', 98);


#######################################################
/* * ********* CSS merge and minify process *********** */
#######################################################

if ($_SERVER['HTTP_HOST'] == 'goldenwing.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
    require( 'includes/class.magic-min.php' );
    $minified = new Minifier(
            array(
        'echo' => false
            )
    );
    //exclude example
    $css_exclude = array(
            TEMPLATEPATH . '/assets/css/combine/superfish-navbar.css', 
            TEMPLATEPATH . '/assets/css/combine/superfish.css'
    );
    //order example
    $css_order = array(
            //TEMPLATEPATH . '/assets/css/reset.css',     
    );

    $minified->merge(TEMPLATEPATH . '/assets/css/combine.min.css', TEMPLATEPATH . '/assets/css/combine', 'css', $css_exclude, $css_order);
}

function the_template_url() {
    echo TEMPLATE_URL;
}

################################################################################
// Enqueue Scripts
################################################################################

function init_scripts() {
    wp_deregister_script('wp-embed');
    wp_deregister_script('jquery');
    wp_deregister_script('comment-reply');
}

function add_scripts() {
    $js_path = ASSET_URL . 'js';
    $css_path = ASSET_URL . 'css';
    if ($_SERVER['HTTP_HOST'] == 'goldenwing.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'bootstrap',
                'src' => $js_path . '/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'jquery.prettyphoto',
                'src' => $js_path . '/jquery.prettyphoto.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ), 
            array(
                'name' => 'plugins',
                'src' => $js_path . '/plugins.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),  
            // array(
            //     'name' => 'superfish',
            //     'src' => $js_path . '/superfish.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(
            //     'name' => 'supersubs',
            //     'src' => $js_path . '/supersubs.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(
            //     'name' => 'easy-tabs',
            //     'src' => $js_path . '/easyResponsiveTabs.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(
            //     'name' => 'mydatepicker',
            //     'src' => $js_path . '/jquery.ui.datepicker.min.js',
            //     'dep' => 'jquery',
            //     'ver' => ASSET_VERSION,
            // ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => $css_path . '/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name' => 'google-font',
                'src' => '//fonts.googleapis.com/css?family=Lora|Open+Sans',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),   
            array(
                'name' => 'font-awesomes',
                'src' => $css_path . '/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),  
            
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
        );
    } else {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => '//cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js',
            ),
            array(
                'name' => 'bootstrap',
                'src' => '//cdn.jsdelivr.net/bootstrap/3.3.6/js/bootstrap.min.js',
            ), 
            array(
                'name' => 'jquery.prettyphoto',
                'src' =>  'https://cdnjs.cloudflare.com/ajax/libs/prettyPhoto/3.1.6/js/jquery.prettyPhoto.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),          
            array(
                'name' => 'plugins',
                'src' => $js_path . '/plugins.js',
                'dep' => 'jquery',
                'ver' => ASSET_VERSION,
                'is_footer' => true
            ),
            array(
                'name'      => 'script',
                'src'       => $js_path . '/script.js',
                'dep'       => 'jquery',
                'ver'       => ASSET_VERSION,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => '//cdn.jsdelivr.net/bootstrap/3.3.6/css/bootstrap.min.css'
            ),
            array(
                'name'  => 'font-awesomes',
                'src'   => '//cdn.jsdelivr.net/fontawesome/4.6.1/css/font-awesome.min.css'
            ),
            array(
                'name' => 'google-font',
                'src' => '//fonts.googleapis.com/css?family=Lora|Open+Sans',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name'  => 'style',
                'src'   => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'all'
            ),
        );
    }
    foreach ($js_libs as $lib) {
        wp_enqueue_script($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['is_footer']);
    }
    foreach ($css_libs as $lib) {
        wp_enqueue_style($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['media']);
    }
}

function my_deregister_scripts() {
    global $post_type;

    if (!is_front_page() && !is_home()) {
        
    }
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

function my_deregister_styles() {
    global $post_type;
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

if (!is_admin())
    add_action('init', 'init_scripts', 10);
add_action('wp_enqueue_scripts', 'add_scripts', 10);
add_action('wp_enqueue_scripts', 'my_deregister_scripts', 100);
add_action('wp_enqueue_scripts', 'my_deregister_styles', 100);



################################################################################
// Add theme support
################################################################################

add_theme_support('automatic-feed-links');
add_theme_support('nav-menus');
add_post_type_support('page', 'excerpt');
add_theme_support('post-thumbnails', array('post', 'page', GW_DESTINATION , GW_TOUR_PROGRAM));

if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'main' => 'Main',
                'mobile' => 'Mobile',
                'footer' => 'Footer',
                'helpful' => 'Helpful Info',
            )
    );
}

################################################################################
// Add theme sidebars
################################################################################

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('Main - Sidebar'),
        'id' => 'main-sidebar-widget-area',
        'description' => 'Widgets in this area will be shown on the right sidebar of default page',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ));
   register_sidebar(array(
       'name' => __('Footer - Sidebar'),
       'id' => 'footer-widget-area',
       'description' => 'Widgets in this area will be shown on the footer region',
       'before_widget' => '',
       'after_widget' => '',
       'before_title' => '',
       'after_title' => '',
   ));
   register_sidebar(array(
       'name' => __('Tour Sidebar - Sidebar'),
       'id' => 'tour-widget-area',
       'description' => 'Widgets in this area will be shown on the rihgt side of tour',
       'before_widget' => '',
       'after_widget' => '',
       'before_title' => '',
       'after_title' => '',
   ));
   register_sidebar(array(
       'name' => __('Destination - Sidebar'),
       'id' => 'destination-widget-area',
       'description' => 'Widgets in this area will be shown on the left-hand side of destination page',
       'before_widget' => '',
       'after_widget' => '',
       'before_title' => '',
       'after_title' => '',
   ));
//    register_sidebar(array(
//        'name' => __('Footer Middle - Sidebar'),
//        'id' => 'footer-middle-widget-area',
//        'description' => 'Widgets in this area will be shown in the middle of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Right - Sidebar'),
//        'id' => 'footer-right-widget-area',
//        'description' => 'Widgets in this area will be shown on the right-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
}





################################################################################
// Comment formatting
################################################################################

function theme_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li>
        <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <header class="comment-author vcard">
    <?php echo get_avatar($comment, $size = '48', $default = '<path_to_url>'); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                <time><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a></time>
                <?php edit_comment_link(__('(Edit)'), '  ', '') ?>
            </header>
                <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br />
            <?php endif; ?>

    <?php comment_text() ?>

            <nav>
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </nav>
        </article>
        <!-- </li> is added by wordpress automatically -->
                <?php
            }
            

//post per page
function progress_archive( $query ) {

     if( $query->is_main_query() && !is_admin() && is_post_type_archive( GW_TOUR_PROGRAM ) || is_tax() ) {
        $query->set( 'posts_per_page', '10' );
    }
    
}
add_action( 'pre_get_posts', 'progress_archive' ); 

################################################################################
// Pagination
################################################################################
if ( ! function_exists( 'dd_pagination' ) ) :
    function dd_pagination() {
        global $wp_query;

        $big = 999999999; // need an unlikely integer
        
        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
                        'show_all' => false,
                        'end_size' => 1,
                        'mid_size' => 2,
                        'prev_next' => true,
                        'prev_text' => __('First'),
                        'next_text' => __('Last'),
                ) );
    }
endif;

function ajax_sendmail_func(){
    
    GLOBAL $THEME_OPTIONS;                         
        
        if( is_array($_POST['lang_require']) ){
            $languages = implode(',', $_POST['lang_require']);
        }else{
            $languages = $_POST['lang_require'];
        }
        
        
        $customize_body = "Tour Program Name : " . $_POST['tour_title'] . "<br/>
                   Arrival Date  : ". $_POST['arrivaldate'] . "<br/>
                   Depature Date  : ". $_POST['depaturedate'] . "<br/>
                   Other Interested Places : ". $_POST['interested_places'] . "<br/>
                   Hotel Accommodation  : ". $_POST['hotel_accommodation'] ."<br/>
                   Hotel Type  : ". $_POST['hotel_star'] . " star<br/>
                   Target Rate : ". $_POST['target_rate'] . " US$ <br/>
                   Flight to and from Destination(s)  : ". $_POST['flight_des'] ."<br/>
                   Tour Guide  : ". $_POST['tour_guide'] . " ( " . $_POST['guide_type'] . " ) <br/>
                   Language  : ". $languages ."<br/><br/>";
    
        $body = "Tour Reservation Form <br/><br/>
                Tour Information<br/>" . $customize_body ."
                Personal Information<br/>
                Name  : ". $_POST['title'] . " ". $_POST['contactName'] . "<br/>
                Email  : " . $_POST['email'] . "<br/>
                Phone  : " . $_POST['phone'] . "<br/>
                Nationality  : ". $_POST['nationality'] . "<br/>
                Sex  : " . $_POST['sex'] . "<br/>
                Address  : " . $_POST['address'] . "<br/>
                Passport No  : ". $_POST['passportno'] . "<br/>
                Adult  : " . $_POST['adult'] . "<br/>
                Child  : ". $_POST['child'] . "<br/>
                Infants  : ". $_POST['infants'] . "<br/>
                Additional Message  : ". $_POST['add_msg'] . "<br/>";
                $subject = "Tour Reservation";
        $to = $THEME_OPTIONS['reservation_form_email'];
                 
                $content .= "<html>\n";
                $content .= "<body style=\"font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; color:#666666;\">\n";
                $body    = html_entity_decode($body);
                $content .= $body;
                $content .= "\n\n";
                $content .= "</body>\n";
                $content .= "</html>\n";

                $headers[] = 'From: Golden Wings Travel and Tour <noreply@naviplustravel.com>';
                $headers[] = "MIME-Version: 1.0\n";
                $headers[] = "Content-type: text/html; charset=UTF-8\r\n";            


                if( !$to ){  // use admin mail if there is no reservation form mail
                    $to = get_option('admin_email');
                }else{
                    if( strpos($to, ',') ){ // if it is comman separated, changed to array
                        $to = explode(',', str_replace(' ', '', $to));
                    }
                }                  
//                echo $to, $subject, $content, $headers;           
               //if( 1 == 1 ){                    
                if(wp_mail($to, $subject, $content, $headers)){
                    $data['status']= 'yes';
                }else{
                    $data['status']= 'no';
                }
                echo json_encode($data);
                die;
                
}

add_action('wp_ajax_sendmail', 'ajax_sendmail_func');           // for logged in user  
add_action('wp_ajax_nopriv_sendmail', 'ajax_sendmail_func');

//ajax captcha check
function ajax_check_captcha() {

    $data = array();

    if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['value'])) != $_SESSION['captcha']) {
        $data['status'] = 'error';
        $data['valid'] = 'no';
    } else {
        $data['status'] = 'success';
        $data['valid'] = 'yes';
    }
    
    echo json_encode($data);    
    die;
}

// my test

// // Add some text after the header
// add_action( 'change_terms' , 'change_related_term' );
// function change_related_term() {
//     $term="mine";
//   echo $term;
// }

pll_register_string('Follow Us On Facebook', 'Follow Us On Facebook'); 
pll_register_string('Tour Code', 'tour_code'); 
pll_register_string('Tour Duration', 'tour_duration'); 
pll_register_string('Tour Type', 'tour_type'); 
pll_register_string('Price', 'price'); 
pll_register_string('Tour Detail', 'tour_detail'); 
pll_register_string('Overview', 'over_view'); 
  

function tml_registration_errors( $errors ) {
	if ( empty( $_POST['phone_no'] ) )
            $errors->add( 'empty_phone_no', '<strong>ERROR</strong>: Please enter your phone number.' );
        
        if ( empty( $_POST['agent_type'] ) )
            $errors->add( 'empty_agent_type', '<strong>ERROR</strong>: Please enter agent type.' );
        
	return $errors;
}
add_filter( 'registration_errors', 'tml_registration_errors' );

function tml_user_register( $user_id ) {
	if ( !empty( $_POST['phone_no'] ) )
		update_user_meta( $user_id, 'phone_no', $_POST['phone_no'] );	
        
        if ( !empty( $_POST['passport'] ) )
		update_user_meta( $user_id, 'passport', $_POST['passport'] );
        
        if ( !empty( $_POST['country'] ) )
		update_user_meta( $user_id, 'country', $_POST['country'] );	
        
        if ( !empty( $_POST['agent_type'] ) )
		update_user_meta( $user_id, 'agent_type', $_POST['agent_type'] );
        
        if ( !empty( $_POST['address'] ) )
		update_user_meta( $user_id, 'address', $_POST['address'] );       
}
add_action( 'user_register', 'tml_user_register' );

function modify_contact_methods($profile_fields) {
	// Add new fields
	$profile_fields['phone_no'] = 'Phone Number';       
        $profile_fields['passport'] = 'Passport';
        $profile_fields['country'] = 'Country';
        $profile_fields['agent_type'] = 'Agent Type';
        $profile_fields['address'] = 'Address';
	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');


//add tour title list in CR7
wpcf7_add_form_tag('tour_list', 'wpcf7_tour_list_shortcode_handler', true);

function wpcf7_tour_list_shortcode_handler($tag) {
//    var_dump($tag);
    if (!is_object($tag)){ return '';}
    global $post;
    ob_start(); 
    $tour_lists = null;
    $tour_args = array(
        'post_type' => GW_TOUR_PROGRAM,
        'posts_per_page' => -1
    );
    $tour_lists = get_posts($tour_args);
?>
        <select name="tour_name" class="form-control cruise_name_dynamic" id="tour_title">
            <option>Select Tour Program</option>
            <?php foreach ($tour_lists as $tour_list) {
                    if(isset($_REQUEST['tourbook_name']) AND $_REQUEST['tourbook_name']==$tour_list->ID)$select = "selected='selected'"; else $select="";
                    echo "<option value='" .$tour_list->post_title."'".$select.">".$tour_list->post_title."</option>";
                }
            ?>
        </select>
<?php
    
    $custom_tour_list = ob_get_clean();
    return $custom_tour_list;
}
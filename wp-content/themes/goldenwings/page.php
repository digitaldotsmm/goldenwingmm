<?php get_header(); ?>
<div class="container">
    <div id="content">
        <div class="inner-padding">
            <div class="row">
                <div class="col-md-8">
                    <?php 
                        $air_line= pll_get_post(GW_FLIGHT_INFORMATIONS);
//                        var_dump($air_line);
                        $air_line_id = get_post( $air_line ); 
                        
                    ?>
                    <h1><?php echo $post->post_title; ?></h1>
                    <div class="post_content <?php if(is_page($air_line_id)){echo 'flight_info';};?>"><?php echo apply_filters('the_content',$post->post_content); ?></div>
                    <div class="clear"></div>
                </div><!-- /col - 8 end -->
                <?php get_sidebar();?>
            </div> <!-- Row End -->
        </div>
    </div>
</div>
	<!-- /container -->
<?php get_footer(); ?>
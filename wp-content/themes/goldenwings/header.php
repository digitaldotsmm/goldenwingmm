<?php global $THEME_OPTIONS; $current_language = pll_current_language();  ?>
<!doctype html>
<!--[if lt IE 7 ]>	<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>		<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>		<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>		<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en"  class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<?php if ( file_exists(TEMPLATEPATH .'/favicon.ico') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php if( !is_search() )echo $body_classes; ?>">

    <div id="main">
        <header>
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-8 col-lg-8 mobile_col">                          
                            <?php 
                                if(pll_current_language()=='en') {
                                    $info_hotline=$THEME_OPTIONS['info_hotline'];  
                                    if( strpos($info_hotline, ',') ){ // if it is comman separated, changed to array
                                        $info_hotline = explode(',', str_replace(' ', '', $info_hotline));
                                    }
                                }else {
                                    $info_hotline=$THEME_OPTIONS['info_hotline_mm'];           
                                }                               
                            ?>
                            <div><i class="fa fa-phone" aria-hidden="true"></i> 
                                <?php if(is_array($info_hotline) && count($info_hotline) > 1) {?>
                                    <?php for($p=0;$p<count($info_hotline);$p++) {?>
                                        <a href="tel:<?php echo $info_hotline[$p];?>"><?php echo $info_hotline[$p];?></a>
                                        <?php echo ($p+1 == count($info_hotline))?'':','?>
                                    <?php }?>
                                <?php }else {echo $info_hotline;}?>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 mobile_col mobile-none">
                            <div class="pull-right social-facebook"><a href="<?php echo $THEME_OPTIONS['facebookid'] ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Like us on Facebook</a></div>
                        </div>

                        <div class="col-xs-2 col-sm-4 col-md-2 col-lg-2 lang_flag text-right mobile_col">
                            <ul><?php pll_the_languages(array('show_flags'=>1,'show_names'=>0)); ?></ul>
                        </div>
                    </div>
                </div>              
            </div>
            <!-- end header top -->
            <?php
                if(!empty($THEME_OPTIONS['logo'])){
                    $logo = $THEME_OPTIONS['logo'];
                }else{
                    $logo = ASSET_URL.'images/logo.png';
                }
            ?>
            <div class="header-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="logo-nav">
                                <div class="logo">
                                    <a href="<?php echo WP_HOME;?>" title="<?php bloginfo('name');?>">
                                        <img src="<?php echo $logo; ?>" alt="<?php bloginfo('name');?>" />
                                    </a>
                                </div>
                                     <?php 
                                        wp_nav_menu( array( 
                                            'menu' => 'Main menu',
                                            'theme_location'  => 'main',
                                            'container_id'    => 'cssmenu'
                                            )); 
                                    ?>
                            </div>
                        </div>  
                    </div>
                </div>              
            </div>
            <!-- end header nav -->
        </header>
        <!-- header -->
	
<footer> <?php global $THEME_OPTIONS; ?>
            <div class="container">
                <div class="footer-top">
                    <div class="row">
                        <?php dynamic_sidebar('footer-widget-area'); ?>
                    </div>
                </div>
                <!-- end Footer top -->
                <div class="footer-copyright">
                
                    <div class="row">
                        <div class="col-xs-12"> 
                            <div class="footer-menu">

                             <?php 
                                  wp_nav_menu( array( 

                                      'menu' => 'Footer Menu',
                                      'theme_location'  => 'footer',
                                      )); 
                                    ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12"> 
                            <div class="footer-social">
                                <ul>                                                             
                                    <?php if($THEME_OPTIONS['facebookid']) { ?><li><a href="<?php echo $THEME_OPTIONS['facebookid'];?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li> <?php }?>
                                    <?php if($THEME_OPTIONS['twitterid']) { ?><li><a href="<?php echo $THEME_OPTIONS['twitterid'];?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li> <?php }?>
                                    <?php if($THEME_OPTIONS['weixinid']) { ?><li><a href="<?php echo $THEME_OPTIONS['weixinid'];?>" target="_blank"><i class="fa fa-weixin" aria-hidden="true"></i></a></li> <?php }?>
                                    <?php if($THEME_OPTIONS['whatappid']) { ?><li><a href="<?php echo $THEME_OPTIONS['whatappid'];?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li> <?php }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12"> 
                            <div class="copyright">
                                <p>Copyright &COPY; <?php echo date('Y'); ?> All Right Reserved. Created by <a href="http://www.digitaldots.com.mm" target="_blank" title="Web Development in Yangon: Digital Dots">Digital Dots</a></p>
                            </div>
                        </div>
                    </div> 
                </div>
                <!-- end Footer copyright -->
            </div>
            
        </footer>
        <!-- end footer -->
    </div>
<?php wp_footer(); ?>
</body>
</html>
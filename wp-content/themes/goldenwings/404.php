<?php get_header(); ?>
<div class="container">
	<div id="content">
		<div class="inner-padding">
			<div class="row">
				<div class="col-md-8">
					<h1><?php _e("Search Results"); ?></h1>	
					<?php if( $_REQUEST['calcat'] ) : ?>
						<p>No event is found.</p>
					<?php else: ?>
						<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.' ); ?></p>
						<?php get_search_form(); ?>	
					<?php endif; ?>
					<div class="clear"></div>
				</div><!-- /col - 8 end -->
				<?php get_sidebar();?>
			</div> <!-- Row End -->
		</div>
	</div>
</div>
	<!-- /container -->
<?php get_footer(); ?>
	
<div class="col-md-4 col-sm-12 col-xs-12 right-aside">

	<?php
        if(is_singular(GW_DESTINATION)) {
            dynamic_sidebar('destination-widget-area');
        }
        elseif(is_singular(GW_TOUR_PROGRAM)){
            dynamic_sidebar('tour-widget-area');
        }
        else {
            dynamic_sidebar('main-sidebar-widget-area');
        }
	?>

</div><!-- /col - 4 end -->


<?php get_header(); ?>
<div class="container">
    <div id="content">
        <div class="inner-padding">
            <div class="row">
                <div class="col-md-8">
                <h1><?php echo $post->post_title; ?></h1>
                    <?php 
                        $img= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                        $img_url= aq_resize($img[0],696,400,true,true,true);
                        $metas = get_fields($post->ID);
                        $key_attractions = $metas['key_attractions'];
                        $tour_gals = $metas['gallery'];
                    ?>
                        <p><img src="<?php  echo $img_url; ?>" alt="<?php echo $post->post_title;?>" class="img-responsive des_single"></p>
                        <div class="post_content"><?php echo apply_filters('the_content',$post->post_content); ?></div>
                        <?php if($key_attractions){?>
                            <h3 class="title-style-1">Key Attractions</h3>
                            <ul class="features">
                                <?php 
                                    foreach ($key_attractions as $key => $key_attraction) {
                                        echo '<li>'.$key_attraction['title'].'</li>';
                                    }
                                ?>
                            </ul>
                        <?php }?>
                        <?php if($tour_gals) {?>
                            <div class="tour_gallery">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="title-style-1">Gallery</h3>
                                        <div class="row">
                                            <?php 								
                                                foreach($tour_gals as $tour_gal):	                        	
                                                    $gal_imgs=aq_resize($tour_gal["url"],768,575,true,true,true);
                                            ?>                         					                                                
                                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                                        <div class="gallery-item">
                                                            <a href="<?php echo $tour_gal["url"]; ?>" rel="prettyPhoto[desgal]" class="prettyPhoto" title="<?php echo $tour_gal['title']?>">
                                                                <img src="<?php echo $gal_imgs;?>" alt="<?php echo $tour_gal['title']?>" class="img-responsive">                            						
                                                            </a>                           
                                                        </div>
                                                    </div>    
                                            <?php endforeach;?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        <?php }?>
                        <div class="clear"></div>
                    </div><!-- /col - 8 end -->
                <?php get_sidebar();?>
            </div> <!-- Row End -->
        </div>
    </div>
</div><!-- /container -->
<?php get_footer(); 
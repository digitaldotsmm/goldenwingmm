<?php get_header(); ?>
<?php
    $metas = get_fields($post->ID);
    $tour_route = $metas['tour_route'];
    $tour_code = $metas['tour_code'];
    $duration = $metas['duration'];
    $price = $metas['price'];
    $tour_details = $metas['detail'];
    
    $services_included = $metas['services_included'];
    $services_excluded = $metas['services_excluded'];
    $term_and_condition = $metas['term_and_condition'];
    $information_sheet = $metas['information_sheet'];
    $price_plan = $metas['price_plan'];
?>
<div class="container">
    <div id="content">
        <div class="inner-padding single_tour">
            <div class="row">
                <div class="col-md-8">
                    <h1><?php echo $post->post_title; ?></h1>
                    <?php if($tour_route){ ?><h3 class="title-style-2"><span><?php echo $tour_route;?></span></h3><?php }?>
                    <div class="schedule-block">
                        <?php if($tour_code):?>
                        <div class="element">
                            <p class="schedule-title"><?php echo pll__('tour_code')?></p>
                                <span class="schedule-content"><?php echo $tour_code;?></span>
                        </div>
                        <?php endif; ?>

                        <?php if($duration):?>
                        <div class="element">
                                <p class="schedule-title"><?php echo pll__('tour_duration')?></p>
                                <span class="schedule-content"><?php echo $duration;?></span>
                        </div>
                        <?php endif; ?>

                        <div class="element last-ele">
                            <p class="schedule-title"><?php echo pll__('tour_type')?></p>
                            <span class="schedule-content">
                            <?php 
                                $t=1;
                                $tour_terms = wp_get_post_terms($post->ID, GW_TOUR_TYPE_TAXO);
                                $t_count = count($tour_terms);
                                if($tour_terms){foreach ($tour_terms as $key => $tour_term) {	
                                    if ( $tour_term->parent == 0 ) {
                                        if($t_count == $t){
                                            $term_name = $tour_term->name;
                                        }else{
                                            $term_name = $tour_term->name .',';
                                        }
                                        echo $term_name;
                                    }		
                                    $t++;
                                }}								 
                             ?>
                             </span>
                        </div>
                        
                        <?php if($price):?>
                            <div class="element">
                                <p class="schedule-title"><?php echo pll__('price')?></p>
                                <span class="schedule-content">	
                                    <?php
                                        $user = wp_get_current_user();
                                        // var_dump($user->roles[0]);var_dump($user->data->user_login);
                                        if ( $user->roles[0] == 'subscriber') {
                                            $price=get_field('agent_price',$post->ID,true);
                                        }
                                        else {                                                                
                                            $price=get_field('price',$post->ID,true);
                                        }
                                    ?>
                                    $<?php echo $price;?>
                                </span>
                            </div>
                            <?php endif; ?>
                        </div> <!-- schedule-block -->

                        <?php if($post->post_content):?>
                                <div class="entry-content overview-block ">
                                        <h3 class="title-style-3"><?php echo pll__('over_view')?></h3>
                                        <div><?php echo apply_filters('the_content',$post->post_content);?></div>
                                </div>
                        <?php endif;?>

                        <?php 
                            if($tour_details):
                        ?>
                        <div class="entry-content overview-block">
                            <h3 class="title-style-3"><?php echo pll__('tour_detail')?></h3> 
                            <div class="timeline-container">
                                <div class="timeline">
                                    <?php 
                                        foreach ($tour_details as $detailkey => $tour_detail):																			
                                        $img = aq_resize($tour_detail['image'], 268, 179, true, true, true);
                                    ?>
                                        <div class="timeline-block timeline-1">
                                            <div class="timeline-title">
                                                <span>Day <?php echo $detailkey+1;?></span>
                                            </div>
                                            <div class="timeline-content medium-margin-top">
                                                <div class="row">
                                                    <div class="timeline-point" style="top: 125px;"><i class="fa fa-circle-o"></i></div>
                                                    <div class="timeline-custom-col content-col ">
                                                        <div class="timeline-location-block">
                                                            <?php if($tour_detail['title']){?><p class="location-name"><?php echo $tour_detail['title'];?><i class="fa fa-map-marker icon-marker"></i></p><?php }?>
                                                            <div class="timeline-image-block">
                                                                <a href="<?php  echo $tour_detail['image']; ?>" title="<?php echo $tour_detail['title'];?>" rel="prettyPhoto[tourgal]"  class="prettyPhoto">
                                                                    <img src="<?php  echo $img; ?>" alt="<?php echo $tour_detail['title'];?>">
                                                                </a>
                                                            </div>
                                                            <div class="description"> <?php echo $tour_detail['content'];?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>	
                                    <?php  endforeach;?>							
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                        
                        <?php if($services_included || $services_excluded || $term_and_condition || $information_sheet || $price_plan ): ?>
                        <div class="entry-content overview-block ">
                            <h3 class="title-style-3">About Tour</h3>
                            <div id="tourtab" data-type="default">          
                                <ul class="resp-tabs-list">                     	
                                    <?php if($services_included): ?><li><span>Service Included</span></li><?php endif;?>
                                    <?php if($services_excluded): ?><li><span>Services Excluded</span></li><?php endif;?>
                                    <?php if($term_and_condition): ?><li><span>Term and Condition</span></li><?php endif;?>
                                    <?php if($information_sheet): ?><li><span>Information Sheet</span></li><?php endif;?>	 
                                    <?php if($price_plan): ?><li><span>Price Plan</span></li><?php endif;?>	                    		 		                    	                    	
                                </ul>
                                <div class="resp-tabs-container">                                                        
                                    <?php if($services_included): ?><div><div class="tour_service"><?php echo $services_included;?></div></div><?php endif;?>
                                    <?php if($services_excluded): ?><div><div class="tour_service"><?php echo $services_excluded;?></div></div><?php endif;?>
                                    <?php if($term_and_condition): ?><div><div class="tour_service"><?php echo $term_and_condition;?></div></div><?php endif;?>
                                    <?php if($information_sheet): ?><div><div class="tour_service"><?php echo $information_sheet;?></div></div><?php endif;?>	
                                    <?php if($price_plan): ?><div><div class="tour_service"><?php echo $price_plan;?></div></div><?php endif;?>	                    	
                                </div>
                            </div>
                        </div>
                        <?php endif;?>

                        <div class="single_book">
                            <form action="<?php echo get_permalink(pll_get_post(GW_RESERVATION)); ?>" method="post">                           
                                <input type="hidden" value="<?php echo $post->ID; ?>" name="tourbook_name" >
                                <input class="single_form btn btn-blue" name="submit" type="submit" id="submit" value="Book this tour">
                            </form>
                        </div>
                        <div class="clear"></div>
                </div><!-- /col - 8 end -->
                <?php get_sidebar();?>
            </div> <!-- Row End -->
        </div>
    </div>
</div><!-- /container -->
<?php get_footer(); 
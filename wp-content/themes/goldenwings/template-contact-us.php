<?php /* Template Name: Contact Us */ get_header(); ?>
<div class="container">
	<div id="content">
		<div class="inner-padding">
		<div class="row">
			<div class="col-md-8">
			<h1><?php echo $post->post_title; ?></h1>
				<?php echo apply_filters('the_content',$post->post_content); ?>
				
			<div class="clear"></div>
		</div><!-- /col - 8 end -->

		<div class="col-md-4 right-aside">
			<h3>Contact Form</h3>
			<?php echo do_shortcode('[contact-form-7 id="942" title="Contact form 1"]'); ?>
		</div><!-- /col - 4 end -->
		</div> <!-- Row End -->
	</div>
	</div>
	</div>
<?php get_footer(); ?>

<?php get_header(); ?> hi
<div class="container tour-taxo">
	<div id="content">
		<div class="inner-padding">
		
			<?php
				$i=1;
                $wrap_div='<div class="row">';
                if (have_posts()): 
                     
                    echo '<div class="row">';
                 
                    while (have_posts()): the_post();  
                    	$tour_title=get_the_title();
                    	$tour_per=get_permalink();
                    	$tour_img = get_attachment_image_src(get_the_ID(), 'large');	
			  			$tournew_img=aq_resize($tour_img[0],263,197,true,true,true);
        	?>											
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="tour-content">
                                <div class="tour-img">
                                    <a href="<?php echo $tour_per?>"><img class="img-responsive" src="<?php  echo ASSET_URL ?>images/img-tour.jpg" alt=""/></a>
                                </div>
                                <div class="tour-details">
                                    <h3><a href="<?php echo $tour_per;?>"><?php echo $tour_title;?></a></h3>
                                    <span class="price">
                                        $<?php echo get_field('price',$hot_deal_tour->ID,true);?>
                                    </span>
                                </div>
                           </div>
                        </div>
			<?php
//                                  Is this a fourth post? If so, make sure it is not the last post?
                        if ($i % 3 === 0 ) { echo '</div>' . $wrap_div; }
                        $i++;

                     endwhile;
                   
                    echo '</div>';
                   
                endif;
                wp_reset_query();
            ?>
			
		</div>
	</div>
</div>
	<!-- /container -->
<?php get_footer(); ?>
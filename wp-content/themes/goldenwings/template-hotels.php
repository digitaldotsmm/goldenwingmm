<?php //Template Name:Hotels In Myanmar ?>
<?php get_header(); ?>
<div class="container">
	<div id="content">
		<div class="inner-padding">
			<div class="row">
				<div class="col-md-12">
					<h1><?php echo $post->post_title; ?></h1>
					<div id="tourtab" data-type="default" class="hotels">
	                    <ul class="resp-tabs-list">  
	                    	<?php 
	                            $hloactions=  get_terms(GW_HOTEL_LOCATION_TAXO,'hide_empty=0');
	                            if($hloactions): foreach ($hloactions as $skey=>$hloaction):
	                                $term_link= get_term_link($hloaction); 
	                        ?>
	                                <li><?php echo $hloaction->name;?></li>   
	                        <?php           
	                            endforeach;endif;
	                        ?>
	                    </ul>
		                <div class="row resp-tabs-container"> 
		                	<?php          
	                            if($hloactions): foreach ($hloactions as $hloaction):
	                                echo '<div><div class="row">';
	                                    $hotel_arg = array('post_type' => GW_HOTEL, 'posts_per_page' => -1, 'tax_query' => array(array('taxonomy' => GW_HOTEL_LOCATION_TAXO, 'field' => 'slug', 'terms' => $hloaction->slug)));
	                                    $hotel_qry = new WP_Query($hotel_arg);
	                                    if ($hotel_qry->have_posts()): $count = 1; while ($hotel_qry->have_posts()): $hotel_qry->the_post();    
		                                    $img_url=  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
		                                    if($img_url) {$hotelimg=  aq_resize($img_url[0],600,400,true,true,true);}
		                                    else {$hotelimg=ASSET_URL.'images/default.jpg';}  
	                        ?>
		                                    <div class="col-md-4 col-sm-4 each_hotel">
		                                        <div class="column_attr">
		                                            <a href="<?php echo $img_url[0];?>" rel="prettyphoto[fesgal]">
		                                                <img src="<?php echo $hotelimg; ?>" class="hotel_img img-responsive" alt="<?php echo get_the_title(); ?>"/>
		                                            </a>    
		                                            <div class="hotel_datas">
		                                            	<h4><?php echo get_the_title(); ?></h4>
		                                            	<div class="about_fes">
		                                            		<ul>
		                                            		<?php  if(get_field('address')){?><li><i class="fa fa-home"></i><?php  echo get_field('address');?></li><?php }?>                                           		
		                                            		<?php  if(get_field('phone')){?><li><i class="fa fa-phone"></i><?php  echo get_field('phone');?></li><?php }?> 
		                                            		<?php  if(get_field('email')){?><li><i class="fa fa-envelope"></i><?php  echo get_field('email');?></li><?php }?> 
		                                            		<?php  if(get_field('star_rate')){?><li><i class="fa fa-star"></i><?php  echo get_field('star_rate');?></li><?php }?> 
		                                            		<?php  if(get_field('website')){?><li><i class="fa fa-dribbble"></i><?php  echo get_field('website');?></li><?php }?> 
		                                            	</div>  
		                                            </div>                                               	                                                                                             
		                                        </div>
		                                    </div>
		                            <?php if( $count % 3 == 0 ){ echo '</div><!-- end .row --><div class="row">'; } ?>
	                        <?php
	                                $count++;     endwhile; endif;
	                                 echo '</div><!-- end .row --></div>';
	                            endforeach;endif;
	                        ?>
		                </div>
		            </div>
				</div><!-- /col - 8 end -->
				
			</div> <!-- Row End -->
		</div>
	</div>
</div>
	<!-- /container -->
<?php get_footer(); ?>
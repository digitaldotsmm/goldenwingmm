<?php
/*
 * Template Name:Reservation
 */
?>
<?php get_header();?>
<div class="container">
   <div id="content">
        <div class="inner-padding">
            <div class="row">
                <div class="col-md-8">
                    <h1><?php echo $post->post_title; ?></h1>
                    <div class="booking-form-wrapper">
                       <div class="booking-form">
                           <?php 
                                if(pll_current_language()=='en') {
                                    echo do_shortcode('[contact-form-7 id="6067" title="Tour Reservation"]');
                                }else{
                                    echo do_shortcode('[contact-form-7 id="6068" title="Tour Reservation MM"]');
                                }
                            ?>
                       </div>
                    </div>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>	
</div>
<?php get_footer();
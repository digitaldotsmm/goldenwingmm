<?php get_header(); ?>
<div class="container">
	<div id="content">
		<div class="inner-padding">
			<div class="row">
				<div class="col-md-8">
					<h1><?php _e("Search Results"); ?></h1>
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				
						<h4><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
						
						<p><?php echo string_limit_words(strip_tags(get_the_content()), 30) . '...'; ?></p>
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">View More</a>
						<hr/>
					<?php endwhile; else: ?>
						<p>
							<?php _e('Sorry, no posts matched your criteria.'); ?>
						</p>
					<?php endif; ?>
				</div>
				<?php get_sidebar();?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>

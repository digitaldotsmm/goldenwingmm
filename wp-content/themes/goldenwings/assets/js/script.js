/*!
 *      Author: DigitalDots
	name: script.js	
	requires: jquery	
 */

var ddFn = {
    
    init: function(){
        Global = new this.Global();
        Utils = new this.Utils();     
        Tour = new this.Tour();   
        
        Global.__init();        
    },
    

    Global:function(){        
        
        this.__init = function(){  
            // justify height of #hotelstab
            // if( $('#tourtab').length > 0 ){
            //     $('#tourtab').easyResponsiveTabs({
            //        activate: function(){                   
            //            Utils.justify_height('#tourtab .resp-tab-content-active', '.each_hotel');
            //        } 
            //     });

            //     Utils.justify_height('#tourtab .resp-tab-content-active', '.each_hotel');
            // }           
        }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/goldenwings/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }  

          $('#left-slider').tab('show');
                      
    }, // end of Global

     Tour:function(){        
        
        this.__init = function(){             
        }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/goldenwings/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }              
    }, // end of Global

    Utils: function(){
        //this.exist= false;
        this.validate_email = function (email){	
            var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            //var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
            if(filter.test(email)){
                return true;
            }
            else{
                return false;
            }
        };	
	
        this.format_my_date = function(dateStr) {
            return dateStr.replace(/^([a-z]{3})( [a-z]{3} \d\d?)(.*)( \d{4})$/i, '$1,$2$4$3');
        };
	
        this.remove_GMTorUTC = function(dateStr) {
            var noGMT = dateStr.replace(/^(.*)(:\d\d GMT+)(.*)$/i, '$1');  // Trim string like 'Thu Apr 21 2011 14:08:46 GMT+1000 (AUS Eastern Standard Time)'
            var noUTC = noGMT.replace(/^(.*)(:\d\d UTC+)(.*)$/i, '$1');    // Trim string like 'Thu Apr 21 14:08:46 UTC+1000 2011'
            return noUTC;
        };
        
        this.is_user_email_exist = function(email, callback){                 
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_email_exist',
                    'email'  :   email     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){
                        return callback (true);
                    }else{
                        return callback (false);
                    }
                }
            });            
        }; 
        this.is_user_name_exist = function(username, callback){              
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_name_exist',
                    'username'  :   username     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){                          
                        return callback (true);
                    }else{
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_pass_valid = function(username, userpass, callback){           
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data:  {
                    action : "is_password_exist", 
                    'id'   : username,
                    'pass' : userpass
                }, 
                async: false,
                dataType: "json",
                success:function(response){                          
                    if(response){                                 
                        return callback (true);
                    }else{                         
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_logged_in = function(callback){
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_logged_in'                     
                },
                async: false,
                dataType:"json",
                success:function(response){                       
                                               
                    return callback (response.is_logged_in);
                     
                }
            });      
        }  
        this.justify_height = function ($wrappe_name, $item_name) {
            //Justify height                  
            var len = jQuery($wrappe_name).find($item_name).length;            
            
            for (var i = 0; i < len; i = i + 3) {
                var first = jQuery($wrappe_name).find($item_name + ':eq(' + i + ')');
                var second = jQuery($wrappe_name).find($item_name + ':eq(' + (i + 1) + ')');
                if (len > 2) { // if there is only 2 div
                    var third = jQuery($wrappe_name).find($item_name + ':eq(' + (i + 2) + ')');
                    if (third.next('div.clear').length <= 0) {
                        jQuery('<div class="clear"></div>').insertAfter(third);
                    }
                }

                var currentHighest = 0;
                currentHighest = Math.max(jQuery(first).height(), jQuery(second).height());
                if (len > 2) {// if there is only 2 div
                    currentHighest = Math.max(currentHighest, jQuery(third).height());
                }

                jQuery(first).css('min-height', currentHighest);
                jQuery(second).css('min-height', currentHighest);
                jQuery(third).css('min-height', currentHighest);
            }
        }                
    }

};


var Global;
var Utils;
var Tour;

$ = $.noConflict();
$(document).ready(function(){  
    ddFn.init();

    //Menu
            $.fn.menumaker = function(options) {

                var cssmenu = $(this), settings = $.extend({
                  title: "Menu",
                  format: "dropdown",
                  sticky: false
                }, options);

                return this.each(function() {
                  cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
                  $(this).find("#menu-button").on('click', function(){
                    $(this).toggleClass('menu-opened');
                    var mainmenu = $(this).next('ul');
                    if (mainmenu.hasClass('open')) { 
                      mainmenu.hide().removeClass('open');
                    }
                    else {
                      mainmenu.show().addClass('open');
                      mainmenu.css({'position':'absolute','z-index':'99','background':'#eee'});
                      if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                      }
                    }
                  });

                  cssmenu.find('li ul').parent().addClass('has-sub');

                  multiTg = function() {
                    cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                    cssmenu.find('.submenu-button').on('click', function() {
                      $(this).toggleClass('submenu-opened');
                      if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide();
                      }
                      else {
                        $(this).siblings('ul').addClass('open').show();
                      }
                    });
                  };

                  if (settings.format === 'multitoggle') multiTg();
                  else cssmenu.addClass('dropdown');

                  if (settings.sticky === true) cssmenu.css('position', 'fixed');

                  resizeFix = function() {
                    if ($( window ).width() > 991) {
                      cssmenu.find('ul').show();
                    }

                    if ($(window).width() <= 991) {
                      cssmenu.find('ul').hide().removeClass('open');
                    }
                  };
                  resizeFix();
                  return $(window).on('resize', resizeFix);

                });
            };

             $("#cssmenu").menumaker({
                title: "Menu",
                format: "multitoggle"
              });

              $("#cssmenu").prepend("<div id='menu-line'></div>");

                var foundActive = false, activeElement, linePosition = 0, menuLine = $("#cssmenu #menu-line"), lineWidth, defaultPosition, defaultWidth;

                $("#cssmenu > ul > li").each(function() {
                  if ($(this).hasClass('active')) {
                    activeElement = $(this);
                    foundActive = true;
                  }
                });

                if (foundActive === false) {
                  activeElement = $("#cssmenu > ul > li").first();
                }

                defaultWidth = lineWidth = activeElement.width();

                defaultPosition = linePosition = activeElement.position().left;

                menuLine.css("width", lineWidth);
                menuLine.css("left", linePosition);

                $("#cssmenu > ul > li").hover(function() {
                  activeElement = $(this);
                  lineWidth = activeElement.width();
                  linePosition = activeElement.position().left;
                  menuLine.css("width", lineWidth);
                  menuLine.css("left", linePosition);
                }, 
                function() {
                  menuLine.css("left", defaultPosition);
                  menuLine.css("width", defaultWidth);
                });
    
    // date picker
    $('.date-picker').datepicker({
        dateFormat: 'dd / M / yy'
    });
    //table wrap
    $('table').wrap( "<div class='table-responsive'></div>" );
    $('table').addClass('table');
    // Tour Booking form submit    
    var tour_bookingForm = $("#tour-book");

    tour_bookingForm.submit(function(e){
        e.preventDefault();

        if ( do_form_validation(tour_bookingForm) ){
   
            var formData = tour_bookingForm.serializeArray();
   
            formData.push({
                name: 'action',
                value: 'sendmail'
            });

            // send data via ajax
            $.ajax({                                                     
                url: '/wp-admin/admin-ajax.php',
                type:"post",                        
                data: formData,
                async: false,     
                dataType : 'json',
                success:function(response){                                                 
                    if( response.status == 'yes' ){
                        tour_bookingForm.fadeOut('slow');
                        $("#success_reservation").fadeIn();
                    }
                }
            });         

        }
    });

    $("input:radio[id=optionsRadios2]").click(function() {
        $('.inbound_child').hide();
         $('.outbound_child').show();
    });
    
    $("input:radio[id=optionsRadios1]").click(function() {       
         $('.outbound_child').hide();
         $('.inbound_child').show();
    });

    // menu
    if ($(window).width() > 768) {
        $('#menu-main-menu .dropdown').on('mouseover', function(){
            $('.dropdown-toggle', $(this)).next('.dropdown-menu').show();
        }).on('mouseout', function(){            
            $('.dropdown-toggle', $(this)).next('.dropdown-menu').hide();
        });
        
        $('.dropdown-toggle').click(function() {
            if ($(this).next('.dropdown-menu').is(':visible')) {
                window.location = $(this).attr('href');
            }
        });
    }
    else {
        $('.menu-main-menu .dropdown').off('mouseover').off('mouseout');
    }

    // });

    // $("input:radio[name=optionsRadios]").click(function() {

    //     var term_id = $(this).val();
    //      var data = {
    //         action: 'change_terms',
    //         another_par: term_id
    //     };

    //      $.ajax({                                                     
    //             url: '/wp-admin/admin-ajax.php',
    //             type:"post",                        
    //             data: data,
    //             async: false,     
    //             success:function(response){                                                 
    //                 alert(response);                                         
    //             }
    //         });
    // });

    // $('ul.sf-menu').supersubs({

    //    minWidth:    16, // minimum width of sub-menus in em units

    //    maxWidth:    40, // maximum width of sub-menus in em units

    //    extraWidth:  1 // extra width can ensure lines don't sometimes turn over

    //  })

    // .superfish(); // call supersubs first, then superfish

    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false,
        animation_speed: 'normal', // fast/slow/normal 
        slideshow: 5000, // false OR interval time in ms
        autoplay_slideshow: false, // true/false
        opacity: 0.80, // Value between 0 and 1 
        show_title: true, // true/false            
        allow_resize: true, // Resize the photos bigger than viewport. true/false
        default_width: 500,
        default_height: 344,
        counter_separator_label: '/', // The separator for the gallery counter 1 "of" 2
        theme: 'pp_default', // light_rounded / dark_rounded / light_square / dark_square / facebook
        horizontal_padding: 20, // The padding on each side of the picture 
        hideflash: false, // Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto
        wmode: 'opaque', // Set the flash wmode attribute
        autoplay: true, // Automatically start videos: True/False 
        modal: false, // If set to true, only the close button will close the window
        deeplinking: true, // Allow prettyPhoto to update the url to enable deeplinking. 
        overlay_gallery: true, // If set to true, a gallery will overlay the fullscreen image on mouse over 
        keyboard_shortcuts: true, // Set to false if you open forms inside prettyPhoto 
        changepicturecallback: function () {}, // Called everytime an item is shown/changed 
        callback: function () {} // Called when prettyPhoto is closed
    });
    
    //pagination li 
    $('ul.pagination a').wrap('<li></li>');
    $('ul.pagination span').wrap('<li></li>');

    // festival and hotel
    if ( $('#tourtab').length > 0 ) {
        var type_action = $('#tourtab').attr('data-type'); 
        $("#tourtab").easyResponsiveTabs({
            type: type_action, //Types: default, vertical, accordion           
            width: 'auto', //auto or any custom width
            fit: true,   // 100% fits in a container
            closed: false, // Close the panels on start, the options 'accordion' and 'tabs' keep them closed in there respective view types
            activate: function() {},  // Callback function, gets called if tab is switched
            tabidentify: 'tab_identifier_child', // The tab groups identifier *This should be a unique name for each tab group and should not be defined in any styling or css file.
            activetab_bg: '#B5AC5F', // background color for active tabs in this group
            inactive_bg: '#E0D78C', // background color for inactive tabs in this group
            active_border_color: '#9C905C', // border color for active tabs heads in this group
            active_content_border_color: '#9C905C' // border color for active tabs contect in this group so that it matches the tab head border
        });
    }

});

// Do form Validation
function do_form_validation($fm){     
    var valid = true; 
    var $ele = null;      
    var $val = null;           

    $fm.find('.required').each(function(){                   
              
        $ele = jQuery(this); 
        
        $val = $ele.val();   
        
        // reset all input to normal
        $ele.removeClass('invalid');      
                
        // if ther is no vale or just space
        if( $val.length <= 0){               
            $ele.addClass('invalid');
            valid = false;   
        }  
         
        if( $ele.hasClass('phone') && $val != '' ){
            var $Regex = /^[0-9-+]+$/;
            if ( !$Regex.test($val) ){
                $ele.addClass('invalid');                        
                valid = false;
            }                   
        }               
       
                
        if( $ele.hasClass('email') && $val != '' ){                      
            if( !validate_email($val)){                                               
                $ele.addClass('invalid');                        
                valid = false;
            }
        }                     
        
        if( $ele.hasClass('captcha') && $val != '' ){
            is_captcha_correct($val, function(result){                        
                if ( result == 'no'){
                    $ele.addClass('invalid');                          
                    valid =  false;
                }       
            });
        }
    });
    
    if( valid ) return true;
    return false;         
            
}     

function validate_email(email){
    var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    //var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
    if(filter.test(email)){
        return true;
    }
    else{
        return false;
    }
}

function is_captcha_correct (value, callback){
    $.ajax({                                                     
        url: '/wp-admin/admin-ajax.php',
        type:"post",                        
        data: {
            action      :  'check_captcha',
            'value'     : value
        },
        async: false,
        dataType:"json",
        success:function(response){                   

            return callback (response.valid);

        }
    });      
}
/*
 * jQuery Superfish Menu Plugin
 * Copyright (c) 2013 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *	http://www.opensource.org/licenses/mit-license.php
 *	http://www.gnu.org/licenses/gpl.html
 */

// (function ($, w) {
// 	"use strict";

// 	var methods = (function () {
// 		// private properties and methods go here
// 		var c = {
// 				bcClass: 'sf-breadcrumb',
// 				menuClass: 'sf-js-enabled',
// 				anchorClass: 'sf-with-ul',
// 				menuArrowClass: 'sf-arrows'
// 			},
// 			ios = (function () {
// 				var ios = /^(?![\w\W]*Windows Phone)[\w\W]*(iPhone|iPad|iPod)/i.test(navigator.userAgent);
// 				if (ios) {
// 					// tap anywhere on iOS to unfocus a submenu
// 					$('html').css('cursor', 'pointer').on('click', $.noop);
// 				}
// 				return ios;
// 			})(),
// 			wp7 = (function () {
// 				var style = document.documentElement.style;
// 				return ('behavior' in style && 'fill' in style && /iemobile/i.test(navigator.userAgent));
// 			})(),
// 			unprefixedPointerEvents = (function () {
// 				return (!!w.PointerEvent);
// 			})(),
// 			toggleMenuClasses = function ($menu, o, add) {
// 				var classes = c.menuClass,
// 					method;
// 				if (o.cssArrows) {
// 					classes += ' ' + c.menuArrowClass;
// 				}
// 				method = (add) ? 'addClass' : 'removeClass';
// 				$menu[method](classes);
// 			},
// 			setPathToCurrent = function ($menu, o) {
// 				return $menu.find('li.' + o.pathClass).slice(0, o.pathLevels)
// 					.addClass(o.hoverClass + ' ' + c.bcClass)
// 						.filter(function () {
// 							return ($(this).children(o.popUpSelector).hide().show().length);
// 						}).removeClass(o.pathClass);
// 			},
// 			toggleAnchorClass = function ($li, add) {
// 				var method = (add) ? 'addClass' : 'removeClass';
// 				$li.children('a')[method](c.anchorClass);
// 			},
// 			toggleTouchAction = function ($menu) {
// 				var msTouchAction = $menu.css('ms-touch-action');
// 				var touchAction = $menu.css('touch-action');
// 				touchAction = touchAction || msTouchAction;
// 				touchAction = (touchAction === 'pan-y') ? 'auto' : 'pan-y';
// 				$menu.css({
// 					'ms-touch-action': touchAction,
// 					'touch-action': touchAction
// 				});
// 			},
// 			getMenu = function ($el) {
// 				return $el.closest('.' + c.menuClass);
// 			},
// 			getOptions = function ($el) {
// 				return getMenu($el).data('sfOptions');
// 			},
// 			over = function () {
// 				var $this = $(this),
// 					o = getOptions($this);
// 				clearTimeout(o.sfTimer);
// 				$this.siblings().superfish('hide').end().superfish('show');
// 			},
// 			close = function (o) {
// 				o.retainPath = ($.inArray(this[0], o.$path) > -1);
// 				this.superfish('hide');

// 				if (!this.parents('.' + o.hoverClass).length) {
// 					o.onIdle.call(getMenu(this));
// 					if (o.$path.length) {
// 						$.proxy(over, o.$path)();
// 					}
// 				}
// 			},
// 			out = function () {
// 				var $this = $(this),
// 					o = getOptions($this);
// 				if (ios) {
// 					$.proxy(close, $this, o)();
// 				}
// 				else {
// 					clearTimeout(o.sfTimer);
// 					o.sfTimer = setTimeout($.proxy(close, $this, o), o.delay);
// 				}
// 			},
// 			touchHandler = function (e) {
// 				var $this = $(this),
// 					o = getOptions($this),
// 					$ul = $this.siblings(e.data.popUpSelector);

// 				if (o.onHandleTouch.call($ul) === false) {
// 					return this;
// 				}

// 				if ($ul.length > 0 && $ul.is(':hidden')) {
// 					$this.one('click.superfish', false);
// 					if (e.type === 'MSPointerDown' || e.type === 'pointerdown') {
// 						$this.trigger('focus');
// 					} else {
// 						$.proxy(over, $this.parent('li'))();
// 					}
// 				}
// 			},
// 			applyHandlers = function ($menu, o) {
// 				var targets = 'li:has(' + o.popUpSelector + ')';
// 				if ($.fn.hoverIntent && !o.disableHI) {
// 					$menu.hoverIntent(over, out, targets);
// 				}
// 				else {
// 					$menu
// 						.on('mouseenter.superfish', targets, over)
// 						.on('mouseleave.superfish', targets, out);
// 				}
// 				var touchevent = 'MSPointerDown.superfish';
// 				if (unprefixedPointerEvents) {
// 					touchevent = 'pointerdown.superfish';
// 				}
// 				if (!ios) {
// 					touchevent += ' touchend.superfish';
// 				}
// 				if (wp7) {
// 					touchevent += ' mousedown.superfish';
// 				}
// 				$menu
// 					.on('focusin.superfish', 'li', over)
// 					.on('focusout.superfish', 'li', out)
// 					.on(touchevent, 'a', o, touchHandler);
// 			};

// 		return {
// 			// public methods
// 			hide: function (instant) {
// 				if (this.length) {
// 					var $this = this,
// 						o = getOptions($this);
// 					if (!o) {
// 						return this;
// 					}
// 					var not = (o.retainPath === true) ? o.$path : '',
// 						$ul = $this.find('li.' + o.hoverClass).add(this).not(not).removeClass(o.hoverClass).children(o.popUpSelector),
// 						speed = o.speedOut;

// 					if (instant) {
// 						$ul.show();
// 						speed = 0;
// 					}
// 					o.retainPath = false;

// 					if (o.onBeforeHide.call($ul) === false) {
// 						return this;
// 					}

// 					$ul.stop(true, true).animate(o.animationOut, speed, function () {
// 						var $this = $(this);
// 						o.onHide.call($this);
// 					});
// 				}
// 				return this;
// 			},
// 			show: function () {
// 				var o = getOptions(this);
// 				if (!o) {
// 					return this;
// 				}
// 				var $this = this.addClass(o.hoverClass),
// 					$ul = $this.children(o.popUpSelector);

// 				if (o.onBeforeShow.call($ul) === false) {
// 					return this;
// 				}

// 				$ul.stop(true, true).animate(o.animation, o.speed, function () {
// 					o.onShow.call($ul);
// 				});
// 				return this;
// 			},
// 			destroy: function () {
// 				return this.each(function () {
// 					var $this = $(this),
// 						o = $this.data('sfOptions'),
// 						$hasPopUp;
// 					if (!o) {
// 						return false;
// 					}
// 					$hasPopUp = $this.find(o.popUpSelector).parent('li');
// 					clearTimeout(o.sfTimer);
// 					toggleMenuClasses($this, o);
// 					toggleAnchorClass($hasPopUp);
// 					toggleTouchAction($this);
// 					// remove event handlers
// 					$this.off('.superfish').off('.hoverIntent');
// 					// clear animation's inline display style
// 					$hasPopUp.children(o.popUpSelector).attr('style', function (i, style) {
// 						return style.replace(/display[^;]+;?/g, '');
// 					});
// 					// reset 'current' path classes
// 					o.$path.removeClass(o.hoverClass + ' ' + c.bcClass).addClass(o.pathClass);
// 					$this.find('.' + o.hoverClass).removeClass(o.hoverClass);
// 					o.onDestroy.call($this);
// 					$this.removeData('sfOptions');
// 				});
// 			},
// 			init: function (op) {
// 				return this.each(function () {
// 					var $this = $(this);
// 					if ($this.data('sfOptions')) {
// 						return false;
// 					}
// 					var o = $.extend({}, $.fn.superfish.defaults, op),
// 						$hasPopUp = $this.find(o.popUpSelector).parent('li');
// 					o.$path = setPathToCurrent($this, o);

// 					$this.data('sfOptions', o);

// 					toggleMenuClasses($this, o, true);
// 					toggleAnchorClass($hasPopUp, true);
// 					toggleTouchAction($this);
// 					applyHandlers($this, o);

// 					$hasPopUp.not('.' + c.bcClass).superfish('hide', true);

// 					o.onInit.call(this);
// 				});
// 			}
// 		};
// 	})();

// 	$.fn.superfish = function (method, args) {
// 		if (methods[method]) {
// 			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
// 		}
// 		else if (typeof method === 'object' || ! method) {
// 			return methods.init.apply(this, arguments);
// 		}
// 		else {
// 			return $.error('Method ' +  method + ' does not exist on jQuery.fn.superfish');
// 		}
// 	};

// 	$.fn.superfish.defaults = {
// 		popUpSelector: 'ul,.sf-mega', // within menu context
// 		hoverClass: 'sfHover',
// 		pathClass: 'overrideThisToUse',
// 		pathLevels: 1,
// 		delay: 800,
// 		animation: {opacity: 'show'},
// 		animationOut: {opacity: 'hide'},
// 		speed: 'normal',
// 		speedOut: 'fast',
// 		cssArrows: true,
// 		disableHI: false,
// 		onInit: $.noop,
// 		onBeforeShow: $.noop,
// 		onShow: $.noop,
// 		onBeforeHide: $.noop,
// 		onHide: $.noop,
// 		onIdle: $.noop,
// 		onDestroy: $.noop,
// 		onHandleTouch: $.noop
// 	};

// })(jQuery, window);



// /*
//  * Supersubs v0.3b - jQuery plugin
//  * Copyright (c) 2013 Joel Birch
//  *
//  * Dual licensed under the MIT and GPL licenses:
//  * 	http://www.opensource.org/licenses/mit-license.php
//  * 	http://www.gnu.org/licenses/gpl.html
//  *
//  *
//  * This plugin automatically adjusts submenu widths of suckerfish-style menus to that of
//  * their longest list item children. If you use this, please expect bugs and report them
//  * to the jQuery Google Group with the word 'Superfish' in the subject line.
//  *
//  */

// ;(function($){ // $ will refer to jQuery within this closure

// 	$.fn.supersubs = function(options){
// 		var opts = $.extend({}, $.fn.supersubs.defaults, options);
// 		// return original object to support chaining
// 		return this.each(function() {
// 			// cache selections
// 			var $$ = $(this);
// 			// support metadata
// 			var o = $.meta ? $.extend({}, opts, $$.data()) : opts;
// 			// cache all ul elements and show them in preparation for measurements
// 			var $ULs = $$.find('ul').show();
// 			// get the font size of menu.
// 			// .css('fontSize') returns various results cross-browser, so measure an em dash instead
// 			var fontsize = $('<li id="menu-fontsize">&#8212;</li>').css({
// 				'padding' : 0,
// 				'position' : 'absolute',
// 				'top' : '-999em',
// 				'width' : 'auto'
// 			}).appendTo($$)[0].clientWidth; //clientWidth is faster than .width()
// 			// remove em dash
// 			$('#menu-fontsize').remove();
// 			// loop through each ul in menu
// 			$ULs.each(function(i) {	
// 				// cache this ul
// 				var $ul = $(this);
// 				// get all (li) children of this ul
// 				var $LIs = $ul.children();
// 				// get all anchor grand-children
// 				var $As = $LIs.children('a');
// 				// force content to one line and save current float property
// 				var liFloat = $LIs.css('white-space','nowrap').css('float');
// 				// remove width restrictions and floats so elements remain vertically stacked
// 				$ul.add($LIs).add($As).css({
// 					'float' : 'none',
// 					'width'	: 'auto'
// 				});
// 				// this ul will now be shrink-wrapped to longest li due to position:absolute
// 				// so save its width as ems.
// 				var emWidth = $ul[0].clientWidth / fontsize;
// 				// add more width to ensure lines don't turn over at certain sizes in various browsers
// 				emWidth += o.extraWidth;
// 				// restrict to at least minWidth and at most maxWidth
// 				if (emWidth > o.maxWidth)		{ emWidth = o.maxWidth; }
// 				else if (emWidth < o.minWidth)	{ emWidth = o.minWidth; }
// 				emWidth += 'em';
// 				// set ul to width in ems
// 				$ul.css('width',emWidth);
// 				// restore li floats to avoid IE bugs
// 				// set li width to full width of this ul
// 				// revert white-space to normal
// 				$LIs.css({
// 					'float' : liFloat,
// 					'width' : '100%',
// 					'white-space' : 'normal'
// 				})
// 				// update offset position of descendant ul to reflect new width of parent.
// 				// set it to 100% in case it isn't already set to this in the CSS
// 				.each(function(){
// 					var $childUl = $(this).children('ul');
// 					var offsetDirection = $childUl.css('left') !== undefined ? 'left' : 'right';
// 					$childUl.css(offsetDirection,'100%');
// 				});
// 			}).hide();
			
// 		});
// 	};
// 	// expose defaults
// 	$.fn.supersubs.defaults = {
// 		minWidth		: 9,		// requires em unit.
// 		maxWidth		: 25,		// requires em unit.
// 		extraWidth		: 0			// extra width can ensure lines don't sometimes turn over due to slight browser differences in how they round-off values
// 	};
	
// })(jQuery); // plugin code ends


// Easy Responsive Tabs Plugin
// Author: Samson.Onna <Email : samson3d@gmail.com>
(function ($) {
    $.fn.extend({
        easyResponsiveTabs: function (options) {
            //Set the default values, use comma to separate the settings, example:
            var defaults = {
                type: 'default', //default, vertical, accordion;
                width: 'auto',
                fit: true,
                closed: false,
                activate: function(){}
            }
            //Variables
            var options = $.extend(defaults, options);            
            var opt = options, jtype = opt.type, jfit = opt.fit, jwidth = opt.width, vtabs = 'vertical', accord = 'accordion';
            var hash = window.location.hash;
            var historyApi = !!(window.history && history.replaceState);
            
            //Events
            $(this).bind('tabactivate', function(e, currentTab) {
                if(typeof options.activate === 'function') {
                    options.activate.call(currentTab, e)
                }
            });

            //Main function
            this.each(function () {
                var $respTabs = $(this);
                var $respTabsList = $respTabs.find('ul.resp-tabs-list');
                var respTabsId = $respTabs.attr('id');
                $respTabs.find('ul.resp-tabs-list li').addClass('resp-tab-item');
                $respTabs.css({
                    'display': 'block',
                    'width': jwidth
                });

                $respTabs.find('.resp-tabs-container > div').addClass('resp-tab-content');
                jtab_options();
                //Properties Function
                function jtab_options() {
                    if (jtype == vtabs) {
                        $respTabs.addClass('resp-vtabs');
                    }
                    if (jfit == true) {
                        $respTabs.css({ width: '100%', margin: '0px' });
                    }
                    if (jtype == accord) {
                        $respTabs.addClass('resp-easy-accordion');
                        $respTabs.find('.resp-tabs-list').css('display', 'none');
                    }
                }

                //Assigning the h2 markup to accordion title
                var $tabItemh2;
                $respTabs.find('.resp-tab-content').before("<h2 class='resp-accordion' role='tab'><span class='resp-arrow'></span></h2>");

                var itemCount = 0;
                $respTabs.find('.resp-accordion').each(function () {
                    $tabItemh2 = $(this);
                    var $tabItem = $respTabs.find('.resp-tab-item:eq(' + itemCount + ')');
                    var $accItem = $respTabs.find('.resp-accordion:eq(' + itemCount + ')');
                    $accItem.append($tabItem.html());
                    $accItem.data($tabItem.data());
                    $tabItemh2.attr('aria-controls', 'tab_item-' + (itemCount));
                    itemCount++;
                });

                //Assigning the 'aria-controls' to Tab items
                var count = 0,
                    $tabContent;
                $respTabs.find('.resp-tab-item').each(function () {
                    $tabItem = $(this);
                    $tabItem.attr('aria-controls', 'tab_item-' + (count));
                    $tabItem.attr('role', 'tab');

                    //Assigning the 'aria-labelledby' attr to tab-content
                    var tabcount = 0;
                    $respTabs.find('.resp-tab-content').each(function () {
                        $tabContent = $(this);
                        $tabContent.attr('aria-labelledby', 'tab_item-' + (tabcount));
                        tabcount++;
                    });
                    count++;
                });
                
                // Show correct content area
                var tabNum = 0;
                if(hash!='') {
                    var matches = hash.match(new RegExp(respTabsId+"([0-9]+)"));
                    if (matches!==null && matches.length===2) {
                        tabNum = parseInt(matches[1],10)-1;
                        if (tabNum > count) {
                            tabNum = 0;
                        }
                    }
                }

                //Active correct tab
                $($respTabs.find('.resp-tab-item')[tabNum]).addClass('resp-tab-active');

                //keep closed if option = 'closed' or option is 'accordion' and the element is in accordion mode
                if(options.closed !== true && !(options.closed === 'accordion' && !$respTabsList.is(':visible')) && !(options.closed === 'tabs' && $respTabsList.is(':visible'))) {                  
                    $($respTabs.find('.resp-accordion')[tabNum]).addClass('resp-tab-active');
                    $($respTabs.find('.resp-tab-content')[tabNum]).addClass('resp-tab-content-active').attr('style', 'display:block');
                }
                //assign proper classes for when tabs mode is activated before making a selection in accordion mode
                else {
                    $($respTabs.find('.resp-tab-content')[tabNum]).addClass('resp-tab-content-active resp-accordion-closed')
                }

                //Tab Click action function
                $respTabs.find("[role=tab]").each(function () {
                   
                    var $currentTab = $(this);
                    $currentTab.click(function () {
                        
                        var $currentTab = $(this);
                        var $tabAria = $currentTab.attr('aria-controls');

                        if ($currentTab.hasClass('resp-accordion') && $currentTab.hasClass('resp-tab-active')) {
                            $respTabs.find('.resp-tab-content-active').slideUp('', function () { $(this).addClass('resp-accordion-closed'); });
                            $currentTab.removeClass('resp-tab-active');
                            return false;
                        }
                        if (!$currentTab.hasClass('resp-tab-active') && $currentTab.hasClass('resp-accordion')) {
                            $respTabs.find('.resp-tab-active').removeClass('resp-tab-active');
                            $respTabs.find('.resp-tab-content-active').slideUp().removeClass('resp-tab-content-active resp-accordion-closed');
                            $respTabs.find("[aria-controls=" + $tabAria + "]").addClass('resp-tab-active');

                            $respTabs.find('.resp-tab-content[aria-labelledby = ' + $tabAria + ']').slideDown().addClass('resp-tab-content-active');
                        } else {
                            $respTabs.find('.resp-tab-active').removeClass('resp-tab-active');
                            $respTabs.find('.resp-tab-content-active').removeAttr('style').removeClass('resp-tab-content-active').removeClass('resp-accordion-closed');
                            $respTabs.find("[aria-controls=" + $tabAria + "]").addClass('resp-tab-active');
                            $respTabs.find('.resp-tab-content[aria-labelledby = ' + $tabAria + ']').addClass('resp-tab-content-active').attr('style', 'display:block');
                        }
                        //Trigger tab activation event
                        $currentTab.trigger('tabactivate', $currentTab);
                        
                        //Update Browser History
                        if(historyApi) {
                            var currentHash = window.location.hash;
                            var newHash = respTabsId+(parseInt($tabAria.substring(9),10)+1).toString();
                            if (currentHash!="") {
                                var re = new RegExp(respTabsId+"[0-9]+");
                                if (currentHash.match(re)!=null) {                                    
                                    newHash = currentHash.replace(re,newHash);
                                }
                                else {
                                    newHash = currentHash+"|"+newHash;
                                }
                            }
                            else {
                                newHash = '#'+newHash;
                            }
                            
                            history.replaceState(null,null,newHash);
                        }
                    });
                    
                });
                
                //Window resize function                   
                $(window).resize(function () {
                    $respTabs.find('.resp-accordion-closed').removeAttr('style');
                });
            });
        }
    });
})(jQuery);


/* =========================================================
 * bootstrap-datepicker.js 
 * http://www.eyecon.ro/bootstrap-datepicker
 * =========================================================
 * Copyright 2012 Stefan Petre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */
 
!function( $ ) {
	
	// Picker object
	
	var Datepicker = function(element, options){
		this.element = $(element);
		this.format = DPGlobal.parseFormat(options.format||this.element.data('date-format')||'mm/dd/yyyy');
		this.picker = $(DPGlobal.template)
							.appendTo('body')
							.on({
								click: $.proxy(this.click, this),
								mousedown: $.proxy(this.mousedown, this)
							});
		this.isInput = this.element.is('input');
		this.component = this.element.is('.date') ? this.element.find('.add-on') : false;
		
		if (this.isInput) {
			this.element.on({
				focus: $.proxy(this.show, this),
				blur: $.proxy(this.hide, this),
				keyup: $.proxy(this.update, this)
			});
		} else {
			if (this.component){
				this.component.on('click', $.proxy(this.show, this));
			} else {
				this.element.on('click', $.proxy(this.show, this));
			}
		}
		this.minViewMode = options.minViewMode||this.element.data('date-minviewmode')||0;
		if (typeof this.minViewMode === 'string') {
			switch (this.minViewMode) {
				case 'months':
					this.minViewMode = 1;
					break;
				case 'years':
					this.minViewMode = 2;
					break;
				default:
					this.minViewMode = 0;
					break;
			}
		}
		this.viewMode = options.viewMode||this.element.data('date-viewmode')||0;
		if (typeof this.viewMode === 'string') {
			switch (this.viewMode) {
				case 'months':
					this.viewMode = 1;
					break;
				case 'years':
					this.viewMode = 2;
					break;
				default:
					this.viewMode = 0;
					break;
			}
		}
		this.startViewMode = this.viewMode;
		this.weekStart = options.weekStart||this.element.data('date-weekstart')||0;
		this.weekEnd = this.weekStart === 0 ? 6 : this.weekStart - 1;
		this.fillDow();
		this.fillMonths();
		this.update();
		this.showMode();
	};
	
	Datepicker.prototype = {
		constructor: Datepicker,
		
		show: function(e) {
			this.picker.show();
			this.height = this.component ? this.component.outerHeight() : this.element.outerHeight();
			this.place();
			$(window).on('resize', $.proxy(this.place, this));
			if (e ) {
				e.stopPropagation();
				e.preventDefault();
			}
			if (!this.isInput) {
				$(document).on('mousedown', $.proxy(this.hide, this));
			}
			this.element.trigger({
				type: 'show',
				date: this.date
			});
		},
		
		hide: function(){
			this.picker.hide();
			$(window).off('resize', this.place);
			this.viewMode = this.startViewMode;
			this.showMode();
			if (!this.isInput) {
				$(document).off('mousedown', this.hide);
			}
			this.set();
			this.element.trigger({
				type: 'hide',
				date: this.date
			});
		},
		
		set: function() {
			var formated = DPGlobal.formatDate(this.date, this.format);
			if (!this.isInput) {
				if (this.component){
					this.element.find('input').prop('value', formated);
				}
				this.element.data('date', formated);
			} else {
				this.element.prop('value', formated);
			}
		},
		
		setValue: function(newDate) {
			if (typeof newDate === 'string') {
				this.date = DPGlobal.parseDate(newDate, this.format);
			} else {
				this.date = new Date(newDate);
			}
			this.set();
			this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
			this.fill();
		},
		
		place: function(){
			var offset = this.component ? this.component.offset() : this.element.offset();
			this.picker.css({
				top: offset.top + this.height,
				left: offset.left
			});
		},
		
		update: function(newDate){
			this.date = DPGlobal.parseDate(
				typeof newDate === 'string' ? newDate : (this.isInput ? this.element.prop('value') : this.element.data('date')),
				this.format
			);
			this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
			this.fill();
		},
		
		fillDow: function(){
			var dowCnt = this.weekStart;
			var html = '<tr>';
			while (dowCnt < this.weekStart + 7) {
				html += '<th class="dow">'+DPGlobal.dates.daysMin[(dowCnt++)%7]+'</th>';
			}
			html += '</tr>';
			this.picker.find('.datepicker-days thead').append(html);
		},
		
		fillMonths: function(){
			var html = '';
			var i = 0
			while (i < 12) {
				html += '<span class="month">'+DPGlobal.dates.monthsShort[i++]+'</span>';
			}
			this.picker.find('.datepicker-months td').append(html);
		},
		
		fill: function() {
			var d = new Date(this.viewDate),
				year = d.getFullYear(),
				month = d.getMonth(),
				currentDate = this.date.valueOf();
			this.picker.find('.datepicker-days th:eq(1)')
						.text(DPGlobal.dates.months[month]+' '+year);
			var prevMonth = new Date(year, month-1, 28,0,0,0,0),
				day = DPGlobal.getDaysInMonth(prevMonth.getFullYear(), prevMonth.getMonth());
			prevMonth.setDate(day);
			prevMonth.setDate(day - (prevMonth.getDay() - this.weekStart + 7)%7);
			var nextMonth = new Date(prevMonth);
			nextMonth.setDate(nextMonth.getDate() + 42);
			nextMonth = nextMonth.valueOf();
			html = [];
			var clsName;
			while(prevMonth.valueOf() < nextMonth) {
				if (prevMonth.getDay() === this.weekStart) {
					html.push('<tr>');
				}
				clsName = '';
				if (prevMonth.getMonth() < month) {
					clsName += ' old';
				} else if (prevMonth.getMonth() > month) {
					clsName += ' new';
				}
				if (prevMonth.valueOf() === currentDate) {
					clsName += ' active';
				}
				html.push('<td class="day'+clsName+'">'+prevMonth.getDate() + '</td>');
				if (prevMonth.getDay() === this.weekEnd) {
					html.push('</tr>');
				}
				prevMonth.setDate(prevMonth.getDate()+1);
			}
			this.picker.find('.datepicker-days tbody').empty().append(html.join(''));
			var currentYear = this.date.getFullYear();
			
			var months = this.picker.find('.datepicker-months')
						.find('th:eq(1)')
							.text(year)
							.end()
						.find('span').removeClass('active');
			if (currentYear === year) {
				months.eq(this.date.getMonth()).addClass('active');
			}
			
			html = '';
			year = parseInt(year/10, 10) * 10;
			var yearCont = this.picker.find('.datepicker-years')
								.find('th:eq(1)')
									.text(year + '-' + (year + 9))
									.end()
								.find('td');
			year -= 1;
			for (var i = -1; i < 11; i++) {
				html += '<span class="year'+(i === -1 || i === 10 ? ' old' : '')+(currentYear === year ? ' active' : '')+'">'+year+'</span>';
				year += 1;
			}
			yearCont.html(html);
		},
		
		click: function(e) {
			e.stopPropagation();
			e.preventDefault();
			var target = $(e.target).closest('span, td, th');
			if (target.length === 1) {
				switch(target[0].nodeName.toLowerCase()) {
					case 'th':
						switch(target[0].className) {
							case 'switch':
								this.showMode(1);
								break;
							case 'prev':
							case 'next':
								this.viewDate['set'+DPGlobal.modes[this.viewMode].navFnc].call(
									this.viewDate,
									this.viewDate['get'+DPGlobal.modes[this.viewMode].navFnc].call(this.viewDate) + 
									DPGlobal.modes[this.viewMode].navStep * (target[0].className === 'prev' ? -1 : 1)
								);
								this.fill();
								this.set();
								break;
						}
						break;
					case 'span':
						if (target.is('.month')) {
							var month = target.parent().find('span').index(target);
							this.viewDate.setMonth(month);
						} else {
							var year = parseInt(target.text(), 10)||0;
							this.viewDate.setFullYear(year);
						}
						if (this.viewMode !== 0) {
							this.date = new Date(this.viewDate);
							this.element.trigger({
								type: 'changeDate',
								date: this.date,
								viewMode: DPGlobal.modes[this.viewMode].clsName
							});
						}
						this.showMode(-1);
						this.fill();
						this.set();
						break;
					case 'td':
						if (target.is('.day')){
							var day = parseInt(target.text(), 10)||1;
							var month = this.viewDate.getMonth();
							if (target.is('.old')) {
								month -= 1;
							} else if (target.is('.new')) {
								month += 1;
							}
							var year = this.viewDate.getFullYear();
							this.date = new Date(year, month, day,0,0,0,0);
							this.viewDate = new Date(year, month, Math.min(28, day),0,0,0,0);
							this.fill();
							this.set();
							this.element.trigger({
								type: 'changeDate',
								date: this.date,
								viewMode: DPGlobal.modes[this.viewMode].clsName
							});
						}
						break;
				}
			}
		},
		
		mousedown: function(e){
			e.stopPropagation();
			e.preventDefault();
		},
		
		showMode: function(dir) {
			if (dir) {
				this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + dir));
			}
			this.picker.find('>div').hide().filter('.datepicker-'+DPGlobal.modes[this.viewMode].clsName).show();
		}
	};
	
	$.fn.datepicker = function ( option, val ) {
		return this.each(function () {
			var $this = $(this),
				data = $this.data('datepicker'),
				options = typeof option === 'object' && option;
			if (!data) {
				$this.data('datepicker', (data = new Datepicker(this, $.extend({}, $.fn.datepicker.defaults,options))));
			}
			if (typeof option === 'string') data[option](val);
		});
	};

	$.fn.datepicker.defaults = {
	};
	$.fn.datepicker.Constructor = Datepicker;
	
	var DPGlobal = {
		modes: [
			{
				clsName: 'days',
				navFnc: 'Month',
				navStep: 1
			},
			{
				clsName: 'months',
				navFnc: 'FullYear',
				navStep: 1
			},
			{
				clsName: 'years',
				navFnc: 'FullYear',
				navStep: 10
		}],
		dates:{
			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
			daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
			months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		},
		isLeapYear: function (year) {
			return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))
		},
		getDaysInMonth: function (year, month) {
			return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
		},
		parseFormat: function(format){
			var separator = format.match(/[.\/\-\s].*?/),
				parts = format.split(/\W+/);
			if (!separator || !parts || parts.length === 0){
				throw new Error("Invalid date format.");
			}
			return {separator: separator, parts: parts};
		},
		parseDate: function(date, format) {
			var parts = date.split(format.separator),
				date = new Date(),
				val;
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			date.setMilliseconds(0);
			if (parts.length === format.parts.length) {
				for (var i=0, cnt = format.parts.length; i < cnt; i++) {
					val = parseInt(parts[i], 10)||1;
					switch(format.parts[i]) {
						case 'dd':
						case 'd':
							date.setDate(val);
							break;
						case 'mm':
						case 'm':
							date.setMonth(val - 1);
							break;
						case 'yy':
							date.setFullYear(2000 + val);
							break;
						case 'yyyy':
							date.setFullYear(val);
							break;
					}
				}
			}
			return date;
		},
		formatDate: function(date, format){
			var val = {
				d: date.getDate(),
				m: date.getMonth() + 1,
				yy: date.getFullYear().toString().substring(2),
				yyyy: date.getFullYear()
			};
			val.dd = (val.d < 10 ? '0' : '') + val.d;
			val.mm = (val.m < 10 ? '0' : '') + val.m;
			var date = [];
			for (var i=0, cnt = format.parts.length; i < cnt; i++) {
				date.push(val[format.parts[i]]);
			}
			return date.join(format.separator);
		},
		headTemplate: '<thead>'+
							'<tr>'+
								'<th class="prev">&lsaquo;</th>'+
								'<th colspan="5" class="switch"></th>'+
								'<th class="next">&rsaquo;</th>'+
							'</tr>'+
						'</thead>',
		contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
	};
	DPGlobal.template = '<div class="datepicker dropdown-menu">'+
							'<div class="datepicker-days">'+
								'<table class=" table-condensed">'+
									DPGlobal.headTemplate+
									'<tbody></tbody>'+
								'</table>'+
							'</div>'+
							'<div class="datepicker-months">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-years">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
								'</table>'+
							'</div>'+
						'</div>';

}( window.jQuery )
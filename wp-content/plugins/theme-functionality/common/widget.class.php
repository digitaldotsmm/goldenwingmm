<?php 


defined('ABSPATH') or die("Cannot access pages directly.");


/**
 * Only display once
 * 
 * This line of code will ensure that we only run the master widget class
 * a single time. We don't need to be throwing errors.
 */
if (!class_exists('MasterWidgetClass')) :

/**
 * Initializing 
 * 
 * The directory separator is different between linux and microsoft servers.
 * Thankfully php sets the DIRECTORY_SEPARATOR constant so that we know what
 * to use.
 */
defined("DS") or define("DS", DIRECTORY_SEPARATOR);

/**
 * Actions and Filters
 * 
 * Register any and all actions here. Nothing should actually be called 
 * directly, the entire system will be based on these actions and hooks.
 */
add_action( 'widgets_init', 'widgets_init_declare_registered', 1 );

/**
 * Register a widget
 * 
 * @param $widget
 */
function register_master_widget( $widget = null )
{
	global $masterWidgets;
	if (!isset($masterWidgets))
	{
		$masterWidgets = array();
	}
	
	if (!is_array($widget)) return false;
	
	$defaults = array(
		'id' => '1',
		'title' => 'Generic Widget',
		'classname' => $widget['id'],
		'do_wrapper' => true,
		'description' => '',
		'width' => 200,
		'height' => 200,
		'fields' => array(),
	);
	
	$masterWidgets[$widget['id']] = wp_parse_args($widget, $defaults);
	
	return true;
}

/**
 * Get the registered widgets
 * 
 * @return array
 */
function get_registered_masterwidgets()
{
	global $masterWidgets;
	
	if (!did_action('init_master_widgets'))
		do_action('init_master_widgets');
		
	return $masterWidgets;
}

/**
 * Initialize the widgets
 * 
 * @return boolean
 */
function widgets_init_declare_registered()
{
	//initialziing variables
	global $wp_widget_factory;
	$widgets = get_registered_masterwidgets();
	
	//reasons to fail
	if (empty($widgets) || !is_array($widgets)) return false;
	
	foreach ($widgets as $id => $widget)
	{
		$wp_widget_factory->widgets[$id] = new MasterWidgetClass( $widget );
	}
	
	return false;
}

/**
 * Multiple Widget Master Class
 * 
 * This class allows us to easily create qidgets without having to deal with the
 * mass of php code.
 * 
 * @author byrd
 * @since 1.3
 */
class MasterWidgetClass extends WP_Widget
{
	/**
	 * Constructor.
	 * 
	 * @param $widget
	 */
	function MasterWidgetClass( $widget )
	{
		$this->widget = apply_filters('master_widget_setup', $widget);
		$widget_ops = array(
			'classname' => $this->widget['classname'], 
			'description' => $this->widget['description'] 
		);
		$this->WP_Widget($this->widget['id'], $this->widget['title'], $widget_ops);
	}
	
	/**
	 * Display the Widget View
	 * 
	 * @example extract the args within the view template
	 
	 extract($args[1]); 
	 
	 * @param $args
	 * @param $instance
	 */
	function widget($sidebar, $instance)
	{
		//initializing variables
		$widget = $this->widget;
		$widget['number'] = $this->number;
		
		$args = array(
			'sidebar' => $sidebar,
			'widget' => $widget,
			'params' => $instance,
		);
		
		$show_view = apply_filters('master_widget_view', $this->widget['show_view'], $widget, $instance, $sidebar);
		$title = apply_filters( 'master_widget_title', $instance['title'] );
		
		if ( $widget['do_wrapper'] )
			echo $sidebar['before_widget'];
		
		if ( $title && $widget['do_wrapper'] )
			//echo $sidebar['before_title'] . $title . $sidebar['after_title'];
		
		//give people an opportunity
		do_action('master_widget_show_'.$widget['id']);
		
		//load the file if it exists
		if (file_exists($show_view))
			require $show_view;
			
		//call the function if it exists
		elseif (is_callable($show_view))
			call_user_func( $show_view, $args );
			
		//echo if we can't figure it out
		else echo $show_view;
		
		if ($widget['do_wrapper'])
			echo $sidebar['after_widget'];
		
	}
	
	/**
	 * Update from within the admin
	 * 
	 * @param $new_instance
	 * @param $old_instance
	 */
	function update($new_instance, $old_instance)
	{
		//initializing variables
		$new_instance = array_map('strip_tags', $new_instance);
		$instance = wp_parse_args($new_instance, $old_instance);
		
		return $instance;
	}
	
	/**
	 * Display the options form
	 * 
	 * @param $instance
	 */
	function form($instance)
	{
		//reasons to fail
		if (empty($this->widget['fields'])) return false;
		
		$defaults = array(
			'id' => '',
			'name' => '',
			'desc' => '',
			'type' => '',
			'options' => '',
			'std' => '',
		);
		
		do_action('master_widget_before');
		foreach ($this->widget['fields'] as $field)
		{
			//making sure we don't throw strict errors
			$field = wp_parse_args($field, $defaults);
			
			$meta = false;
			if (isset($field['id']) && array_key_exists($field['id'], $instance))
				@$meta = attribute_escape($instance[$field['id']]);
			
			if ($field['type'] != 'custom' && $field['type'] != 'metabox') 
			{
				echo '<p><label for="',$this->get_field_id($field['id']),'">';
			}
			if (isset($field['name']) && $field['name']) echo $field['name'],':';
			
			if( $field['terms'] ){
				$options = get_terms(array(EVENT_TAXONOMY, 'category'), 'fields=names');
				$field['options'] = $options;
			}
			switch ($field['type'])
			{
				case 'text':
					echo '<input type="text" name="', $this->get_field_name($field['id']), '" id="', $this->get_field_id($field['id']), '" value="', ($meta ? $meta : @$field['std']), '" class="vibe_text" />', 
					'<br/><span class="description">', @$field['desc'], '</span>';
					break;
				case 'textarea':
					echo '<textarea class="vibe_textarea" name="', $this->get_field_name($field['id']), '" id="', $this->get_field_id($field['id']), '" cols="60" rows="4" style="width:97%">', $meta ? $meta : @$field['std'], '</textarea>', 
					'<br/><span class="description">', @$field['desc'], '</span>';
					break;
				case 'select':
				//var_dump($field['options']);
					echo '<select class="vibe_select" name="', $this->get_field_name($field['id']), '" id="', $this->get_field_id($field['id']), '">';
					foreach ($field['options'] as $option)
					{
						echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
					}
					echo '</select>', 
					'<br/><span class="description">', @$field['desc'], '</span>';
					break;
				case 'radio':
					foreach ($field['options'] as $option)
					{
						echo '<input class="vibe_radio" type="radio" name="', $this->get_field_name($field['id']), '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', 
						$option['name'];
					}
					echo '<br/><span class="description">', @$field['desc'], '</span>';
					break;
				case 'checkbox':
					echo '<input type="hidden" name="', $this->get_field_name($field['id']), '" id="', $this->get_field_id($field['id']), '" /> ', 
						 '<input class="vibe_checkbox" type="checkbox" name="', $this->get_field_name($field['id']), '" id="', $this->get_field_id($field['id']), '"', $meta ? ' checked="checked"' : '', ' /> ', 
					'<br/><span class="description">', @$field['desc'], '</span>';
					break;
				case 'custom':
					echo $field['std'];
					break;
			}
			
			if ($field['type'] != 'custom' && $field['type'] != 'metabox') 
			{
				echo '</label></p>';
			}
		}
		do_action('master_widget_after');
		return;
	}
	
}// ends Master Widget Class

endif; //if !class_exists

//twitter widget output
function twitter_widget_view( $args ){
	extract($args);
	global $post, $artist_twitter_username;
?>	
	 <aside id="twitter" class="widget light">
		<div class="twitterbg"></div>
		<a href="http://twitter.com/<?php echo $params['twitter_id'] ?>/" class="followme" title="Follow us on Twitter">Follow Vivid Sydney on Twitter</a>
		<div class="twitter-feed"></div>
		<script>	
			var _tweeterUserName = '<?php echo $params['twitter_id'] ?>';		
			var _numTweets = '<?php echo $params['num_of_tweets'] ?>';
			 var tweetCount = 1;
			 $(document).ready(function () {
				$('.twitter-feed').jTweetsAnywhere({
					username: _tweeterUserName,
					count: _numTweets, 
					tweetTimestampDecorator: function (tweet, options) {						
						var tweetDate = new Date(Date.parse(Utils.format_my_date(tweet.created_at)));	
						var today = new Date();					
						var utcoffset = Date.today().getUTCOffset(); //it is returning as +1100
						utcoffset = parseInt(utcoffset[1]) * 10 +  parseInt(utcoffset[2]);
						tweetDate = new Date(tweetDate.getTime() + (utcoffset * 60 * 60 * 1000));												
						return ' ' + $.timeago(tweetDate);					
					},
					showTweetFeed: {
						showTimestamp: {
         				   refreshInterval: 15
					    }	
					}
				});
			});
     </script>	
	 </aside>	
<?php
}

// facebook fanbox widget output
function facebook_widget_view( $args ){
	extract($args);
	GLOBAL $THEME_OPTIONS;
?>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
    <h3 class="ft-title"><?php echo pll__('Follow Us On Facebook'); ?></h3>
    <div class="double-solid"></div>
	<div class="fb-page" data-href="<?php echo $THEME_OPTIONS['facebookid'];?>" data-tabs="timeline" data-small-header="false" data-height="200" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="<?php echo $THEME_OPTIONS['facebookid'];?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $THEME_OPTIONS['facebookid'];?>">Golden Wing Myanmar Travel &amp; Tour</a></blockquote></div>
</div>
<?php	
}
//settings for twitter widget.
$twitterWidget = array(
	'id'			=> 'twitter-custom-widget',	//make sure that this is unique
	'title' 		=> '[DOT]Twitter',	
	'description'	=> 'Showing twitter feed',
	'classname'		=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'twitter_widget_view',	
	'fields' => array(
		array(
			'name' 	=> 'Title',
			'desc' 	=> '',
			'id' 	=> 'title',
			'type' 	=> 'text',
			'std' 	=> 'Twitter Feed'
		),
		array(
			'name' 	=> 'Twitter Username',
			'desc' 	=> '',
			'id' 	=> 'twitter_id',
			'type' 	=> 'text',
			'std' 	=> get_theme_option('twitterid')
		),
		array(
			'name' 	=> 'Number of Tweets',
			'desc' 	=> '',
			'id' 	=> 'num_of_tweets',
			'type' 	=> 'text',
			'std' 	=> '4'
		),
	
	)
);
//register this widget
register_master_widget($twitterWidget);

//settings for facebook widget.
$facebookWidget = array(
	'id'			=> 'facebook-custom-widget',	//make sure that this is unique
	'title' 		=> '[DOT]Facebook',	
	'description'	=> 'Facebook Like Box',
	'classname'		=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'facebook_widget_view',
);
//register this widget
register_master_widget($facebookWidget);

// Gallery Widget for footer
function aboutmm_widget_view( $args ){
    extract($args);
    ?>  
    <?php $aboutmm_ids = pll_get_post(GW_ABOUT_MYANMAR , $current_language);
            //var_dump($our_vision);
            $aboutmm = get_page($aboutmm_ids);
       		?>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <h3 class="ft-title"><?php echo $aboutmm->post_title ;?></h3>
        <div class="double-solid"></div>
        <p><?php echo $aboutmm->post_excerpt ;?></p>
        <a class="know-more" href="/<?php echo $aboutmm->post_name ;?>/">KNOW MORE</a>
    </div>
<?php      

}
    
$aboutmmWidget = array(
	'id'		=> 'about_mm_widget',	//make sure that this is unique
	'title' 	=> '[DOT]About Myanmar',		
	'description'	=> 'To Show the excerpt of About Myanmar page Widget',
	'classname'     => 'aboutmm-wrapper',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'aboutmm_widget_view',
        
	
);
//register this widget
register_master_widget($aboutmmWidget);

##################  custom widgets ###################################
// newsletter widget
function newsletter_widget_view( $args ){
	extract($args);
?>
<?php GLOBAL $THEME_OPTIONS; ?>
<div class="widget footer-widgets message-widget">
    <h4><?php echo $params['info_widget_title']; ?></h4>
    <div class="msg"></div>
    <form action="<?php echo WP_HOME ?>" method="post" id="footer-contact" class="contact-work-form">
            <input type="text" name="name" id="name" placeholder="Name" class="required">
            <input type="text" name="mail" id="mail" placeholder="Email" class="required">
            <textarea name="comment" id="comment" placeholder="Message" class="required"></textarea>
            <button type="submit" name="contact-submit" class="submit_contact">
                    <i class="fa fa-envelope"></i> Send
            </button>
    </form>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $("#footer-contact").validate({
            submitHandler: function(form) {                   
                    var $fm = $("#footer-contact")
                    var form_data    = $fm.serialize();
                    var data_string  = 'action=mail_data&' + form_data;
                    $.ajax({
                        type: "POST",
                        url:  "/wp-admin/admin-ajax.php",
                        data: data_string,
                        success: function() {
                            $fm.hide();
                            $(".msg").fadeIn();
                    }
                    });
                    return false;                        
            }

        });
    });
</script> 
<?php 
} 
$newsletterWidget = array(
	'id'			=> 'newsletter-custom-widget',	//make sure that this is unique
	'title' 		=> '[DOT]Newsletter',	
	'description'	=> 'Subscription form',
	'classname'		=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'newsletter_widget_view',
        'fields' => array(
                    array(
                            'name' 	=> 'Title',
                            'desc' 	=> '',
                            'id' 	=> 'info_widget_title',
                            'type' 	=> 'text',
                            'std' 	=> ''
                    )
	)
);
register_master_widget($newsletterWidget);


// Main Info Widget
function info_widget_view( $args ){
	extract($args);
?>
    <?php GLOBAL $THEME_OPTIONS; ?>
    <?php 
        $contact_ids = pll_get_post(GW_CONTACT_US , $current_language);
        $contactid = get_page($contact_ids);
        if(pll_current_language()=='en') {
            $info_address=$THEME_OPTIONS['info_address'];
            $info_phone=$THEME_OPTIONS['info_phone'];           
            if( strpos($info_phone, ',') ){ // if it is comman separated, changed to array
                $info_phone = explode(',', str_replace(' ', '', $info_phone));
            }
        }else {
            $info_address=$THEME_OPTIONS['info_address_mm'];
            $info_phone=$THEME_OPTIONS['info_phone_mm'];           
        }
    ?>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <h3 class="ft-title"><?php echo $contactid->post_title ;?></h3>
        <div class="double-solid"></div>
        <div class="contact">
            <div class="info_address contact_wrap"><span class="c_icon"><i class="fa fa-map-marker"></i></span><span class="c_value"><?php echo $info_address;?></span></div>
            <div class="info_phone contact_wrap">
                <span class="c_icon"><i class="fa fa-phone"></i></span>
                <span class="c_value">
                    <?php if(is_array($info_phone) && count($info_phone) > 1) {?>
                        <?php for($p=0;$p<count($info_phone);$p++) {?>
                            <a href="tel:<?php echo $info_phone[$p];?>"><?php echo $info_phone[$p];?></a>
                            <?php echo ($p+1 == count($info_phone))?'':','?>
                        <?php }?>
                    <?php }else {echo $info_phone;}?>
                </span>
            </div>
            <div class="info_email contact_wrap"><span class="c_icon"><i class="fa fa-envelope"></i></span><span class="c_value"><a href="mailto:<?php echo $THEME_OPTIONS['info_email'];?>"><?php echo $THEME_OPTIONS['info_email'];?></a></span></div>
            <div class="info_host contact_wrap"><span class="c_icon"><i class="fa fa-globe"></i></span><span class="c_value"><a href="/"><?php echo $_SERVER['HTTP_HOST'];?></a></span></div>                              
        </div>
    </div>

<?php 
} 
$infoWidget = array(
	'id'		=> 'info-custom-widget',	//make sure that this is unique
	'title' 	=> '[DOT]Footer Contact Information',	
	'description'	=> 'It will show contact information including phone numbers, fax numbers and address.',
	'classname'	=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'info_widget_view',
        
	
);
register_master_widget($infoWidget);

// Selection page Widget
function travelinfo_widget_view( $args ){
	extract($args);
        
?>
    <?php 
        $travelinfo_ids = pll_get_post(GW_TRAVEL_INFORMATIONS , $current_language);
        $travelinfo_id = get_page($travelinfo_ids);
    ?>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
    <h3 class="ft-title"><?php echo $travelinfo_id->post_title; ?></h3>
    <div class="double-solid"></div>
    <ul>
        <?php 
            $args = array(
                'include'   => array(GW_FLIGHT_INFORMATIONS,GW_TRAVLE_TIPS,GW_DO_DONT,GW_HOTELS_IN_MYANMAR),
                'post_type' => 'page',
                'orderby'   => 'post__in'
             );
                $pages =  get_posts($args);
                foreach ($pages as $pkey=>$page) {  
                    if($pkey==0) { $icon= ASSET_URL.'images/icon-footer-1.jpg';}
                    elseif($pkey==1) {$icon= ASSET_URL.'images/icon-footer-2.jpg';}
                    elseif($pkey==2) {$icon= ASSET_URL.'images/icon-footer-3.jpg';}
                    else {$icon= ASSET_URL.'images/icon-footer-4.jpg';}
        ?>
                    <li>
                        <a href="<?php echo get_the_permalink($page->ID) ;?>"><img src="<?php echo $icon ;?>" alt="Golden Wing Myanmar - <?php echo $page->post_title; ?>" /> <?php echo $page->post_title; ?></a>
                    </li>           
        <?php  } ?>
    </ul>
</div>

<?php 
} 
$infoWidget = array(
	'id'		=> 'travelinfo-custom-widget',	//make sure that this is unique
	'title' 	=> '[DOT]Travel Information',	
	'description'	=> 'It will show travel information including related pages.',
	'classname'	=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'travelinfo_widget_view',
        'fields' => array(
                    array(
                            'name' 	=> 'Title',
                            'desc' 	=> '',
                            'id' 	=> 'travelinfo_widget_title',
                            'type' 	=> 'text',
                            'std' 	=> ''
                    )
        )
	
);
register_master_widget($infoWidget);

// Destination Widget
function destination_widget_view( $args ){
	extract($args);
?>
	<div class="widgets">
		<h3 class="title-style-1"><?php echo $params['destination_widget_title'];?></h3>
		<?php
                if(is_singular(GW_DESTINATION)){$ppp=15;}else{$ppp=5;}
	      	$args = array('post_type' => GW_DESTINATION, 'posts_per_page' => $ppp,'exclude'=> $post->ID);
			$destination_lists = get_posts( $args );
			echo '<ul class="news">';
				foreach ( $destination_lists as $destination_list ) :
		      		echo '<li><a href="'.get_permalink($destination_list->ID).'">'.$destination_list->post_title.'</a></li>';
				endforeach;
			echo '</ul>';
		?>
	</div>
<?php 
} 
$destinationWidget = array(
	'id'		=> 'destination-custom-widget',	//make sure that this is unique
	'title' 	=> '[DOT]Destination Widget',	
	'description'	=> 'It will show destination related pages.',
	'classname'	=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'destination_widget_view',
        'fields' => array(
                    array(
                            'name' 	=> 'Title',
                            'desc' 	=> '',
                            'id' 	=> 'destination_widget_title',
                            'type' 	=> 'text',
                            'std' 	=> ''
                    )
        )
	
);
register_master_widget($destinationWidget);

// Inbound Tour Program Widget
function inbound_widget_view( $args ){
	extract($args);
?>
    <div class="widgets">
        <h3 class="title-style-1"><?php echo $params['inbound_widget_title'];?></h3>	
        <div id="accordion" role="tablist" aria-multiselectable="true">
            <?php 
                $inbound_term_id = pll_get_term(GW_INBOUND_TOUR_TERM_ID);
                $inboundchildren = get_term_children( $inbound_term_id, GW_TOUR_TYPE_TAXO );              
                foreach ( $inboundchildren as $ibkey=>$inboundchild ) :  
                    $ibchild = get_term_by( 'id', $inboundchild, GW_TOUR_TYPE_TAXO );
                    if($ibchild->count > 0){
            ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="<?php echo $ibchild->slug;?>">
                          <h4 class="panel-title">
                            <a  class="accordion-toggle"  data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $ibchild->term_id;?>"  aria-controls="collapse<?php echo $ibchild->term_id;?>">
                              <?php echo $ibchild->name;?>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse<?php echo $ibchild->term_id;?>" class="panel-collapse collapse <?php echo ($ibkey==0)?'in':'';?>" role="tabpanel" aria-labelledby="<?php echo $ibchild->slug;?>">
                            <?php
                                $args = array('post_type' => GW_TOUR_PROGRAM, 'posts_per_page' => 5, 'tax_query' => array(array('taxonomy' => GW_TOUR_TYPE_TAXO, 'field' => 'slug', 'terms' => $ibchild->slug)));
                                $inbound_lists = get_posts( $args );
                                echo '<ul class="news">';
                                        foreach ( $inbound_lists as $inbound_list ) :
                                        echo '<li><a href="'.get_permalink($inbound_list->ID).'">'.$inbound_list->post_title.'</a></li>';
                                        endforeach;
                                echo '</ul>';
                            ?>
                        </div>
                    </div>
            <?php } endforeach;?>
        </div> <!-- accordion wrap -->
    </div>
	<!-- inbound -->
<?php 
} 
$inboundWidget = array(
	'id'		=> 'inbound-custom-widget',	//make sure that this is unique
	'title' 	=> '[DOT] Inbound Tour Programs',	
	'description'	=> 'It will show tour program related pages.',
	'classname'	=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'inbound_widget_view',
        'fields' => array(
                    array(
                            'name' 	=> 'Title',
                            'desc' 	=> '',
                            'id' 	=> 'inbound_widget_title',
                            'type' 	=> 'text',
                            'std' 	=> ''
                    )
        )
	
);
register_master_widget($inboundWidget);

// Outbound Tour Program Widget
function outbound_widget_view( $args ){
    extract($args);
?>
    <!-- outbound -->
    <div class="widgets">
        <h3 class="title-style-1"><?php echo $params['outbound_widget_title'];?></h3>	
        <div id="accordion" role="tablist" aria-multiselectable="true">
            <?php 
                $outbound_term_id = pll_get_term(GW_OUTBOUND_TOUR_TERM_ID);
                $outboundchildren = get_term_children( $outbound_term_id, GW_TOUR_TYPE_TAXO );              
                foreach ( $outboundchildren as $obkey=>$outboundchild ) : 
                    $obchild = get_term_by( 'id', $outboundchild, GW_TOUR_TYPE_TAXO );
                    if($obchild->count > 0){
            ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="<?php echo $obchild->slug;?>">
                          <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $obchild->term_id;?>"  aria-controls="collapse<?php echo $obchild->term_id;?>">
                              <?php echo $obchild->name;?>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse<?php echo $obchild->term_id;?>" class="panel-collapse collapse <?php echo ($obkey==0)?'in':'';?>" role="tabpanel" aria-labelledby="<?php echo $obchild->slug;?>">
                            <?php
                                $args = array('post_type' => GW_TOUR_PROGRAM, 'posts_per_page' => 5, 'tax_query' => array(array('taxonomy' => GW_TOUR_TYPE_TAXO, 'field' => 'slug', 'terms' => $obchild->slug)));
                                    $outbound_lists = get_posts( $args );
                                    echo '<ul class="news">';
                                            foreach ( $outbound_lists as $outbound_list ) :
                                            echo '<li><a href="'.get_permalink($outbound_list->ID).'">'.$outbound_list->post_title.'</a></li>';
                                            endforeach;
                                    echo '</ul>';
                            ?>
                        </div>
                    </div>
            <?php } endforeach;?>
        </div> <!-- accordion wrap -->
    </div>
<?php 
} 
$outboundWidget = array(
	'id'		=> 'outbound-custom-widget',	//make sure that this is unique
	'title' 	=> '[DOT] Outbound Widget',	
	'description'	=> 'It will show tour program related pages.',
	'classname'	=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'outbound_widget_view',
        'fields' => array(
                    array(
                            'name' 	=> 'Title',
                            'desc' 	=> '',
                            'id' 	=> 'outbound_widget_title',
                            'type' 	=> 'text',
                            'std' 	=> ''
                    )
        )
	
);
register_master_widget($outboundWidget);

// Tour Program Widget
function route_map_widget_view( $args ){
	extract($args);
?>
	<?php if(get_field('route_map',$post->ID)): ?>
		<div class="widgets">
			<h3 class="title-style-1"><?php echo $params['route_map_widget_title'];?></h3>	
			<div class="route_map"><a href="<?php echo get_field('route_map',$post->ID); ?>" rel="prettyPhoto[map]" class="prettyPhoto"><img src="<?php echo get_field('route_map',$post->ID);?>" alt="Route Map"></a></div>
		</div>
	<?php endif;?>
<?php 
} 
$route_mapWidget = array(
	'id'		=> 'route_map-custom-widget',	//make sure that this is unique
	'title' 	=> '[DOT] Tour Route Map',	
	'description'	=> 'It will show tour program related pages.',
	'classname'	=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'route_map_widget_view',
        'fields' => array(
                    array(
                            'name' 	=> 'Title',
                            'desc' 	=> '',
                            'id' 	=> 'route_map_widget_title',
                            'type' 	=> 'text',
                            'std' 	=> ''
                    )
        )
	
);
register_master_widget($route_mapWidget);

// Tour Program Gallery Widget
function gallery_widget_view( $args ){
	extract($args);
?>
	<?php if(get_field('gallery',$post->ID)): ?>
		<div class="widgets">
			<h3 class="title-style-1"><?php echo $params['gallery_widget_title'];?></h3>	
			<div class="row">
				<?php 
	    			$tour_gals=get_field('gallery',$post->ID);				
	                if ($tour_gals):          
	                	foreach($tour_gals as $tour_gal):	                        	
				  			$gal_imgs=aq_resize($tour_gal["url"],768,575,true,true,true);
	            ?>                         					                                                
	                        <div class="col-md-12 col-sm-6 col-xs-6 gallery-item">
	                            <!-- <div class="gallery-item"> -->
	            					<a href="<?php echo $tour_gal["url"]; ?>" rel="prettyPhoto[gal]" class="prettyPhoto">
	                                    <img src="<?php echo $gal_imgs;?>" alt="<?php echo $tour_gal['title']?>" class="img-responsive">
	            					</a>                           
	            				<!-- </div> -->
	                        </div>    
	        	<?php endforeach;	 endif;?>
        	</div>
		</div>
	<?php endif;?>
<?php 
} 
$galleryWidget = array(
	'id'		=> 'gallery-custom-widget',	//make sure that this is unique
	'title' 	=> '[DOT] Gallery Widget',	
	'description'	=> 'It will show gallery related pages.',
	'classname'	=> 'st-custom-wi',	
	'do_wrapper' 	=> true,	
	'show_view' 	=> 'gallery_widget_view',
        'fields' => array(
                    array(
                            'name' 	=> 'Title',
                            'desc' 	=> '',
                            'id' 	=> 'gallery_widget_title',
                            'type' 	=> 'text',
                            'std' 	=> ''
                    )
        )
	
);
register_master_widget($galleryWidget);